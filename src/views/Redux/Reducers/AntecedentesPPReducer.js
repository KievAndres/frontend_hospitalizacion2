import { UPDATE_ANTECEDENTESPP } from '../Actions/AntecedentesPPActions';

export default function antecedentesppReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANTECEDENTESPP:
            return payload.antecedentespp;
        default:
            return state;
    }
}