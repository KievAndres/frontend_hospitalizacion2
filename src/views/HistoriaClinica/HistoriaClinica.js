import React, { Component } from 'react';
import {
    Alert,
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import Encabezado from '../Encabezado/Encabezado';
import HabitoFrecuencia from './HabitoFrecuencia';
import PacienteNoAsignado from '../NoCreado/PacienteNoAsignado';
import HCNoCreada from '../NoCreado/HCNoCreada';

import cie10 from '../CIE10/JSON/cie10.json';
import Search from 'react-search-box';

import { connect } from 'react-redux';

import { css } from 'react-emotion';
import { ClipLoader, BounceLoader } from 'react-spinners';
import { Z_BLOCK } from 'zlib';

// TOASTER
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const override = css`
  display: Z_BLOCK;
  margin: 0 auto;
  border-color: red;
  `;

class HistoriaClinica extends Component {
    constructor(props){
        super(props)
        
        var hoy = new Date(),
            f = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

        this.state = {
            afiliadox: [],
            // telefonox:[],
            fecha: f,
            hora: new Date(),
            id_historiac: 'hc'+this.props.paciente.dip,

            consulta: '',
            validaconsulta: true,

            historia: '',
            validahistoria: true,

            datosc: '',

            diagnostico: '',
            validadiagnostico: true,

            id_anamnesis: 'an'+this.props.paciente.dip,
            fecha_actualizacion: f,
            vivienda: 'Propia',
            shi: 'Si',
            aguai: 'Si',

            alimentacion: 'Dieta variada sin predominancia de grupo alimenticio',
            alimentacionrespaldo: '',
            validaalimentacion: true,

            alcohol: 'No Consume',
            acantidad: 1,
            acantidadlabel: 'vez',
            afrecuencia: 'al dia, semana, año...',
            validaalcohol: true,
            
            tabaco: 'No Consume',
            tcantidad: 1,
            tcantidadlabel: 'vez',
            tfrecuencia: 'al dia, semana, año...',
            validatabaco: true,
            
            somnia: 8,
            validasomnia: true,

            diuresis: 3,
            validadiuresis: true,

            hintestinal: 1,
            validahintestinal: true,

            id_app: 'app'+this.props.paciente.dip,
            combe: false,
            combedesc: 'Tuberculosis',
            validacombe: true,

            hipertension: false,
            tbcpulmonar: false,
            diabetes: false,
            pmamaria: false,
            cardiopatias: false,
            ets: false,
            varices: false,
            erenal: false,
            hepatopatias: false,
            tgenitales: false,
            egastrointestinal: false,

            otrossw: false,
            otros: [{enfermedad: ''}],
            validaotros: [{valida: true}],

            aquirurgicos: [{tratamientoq: ''}],
            aquirurgicosano: [{ano: 1900}],
            aquirurgicossw: 'No',
            validaaquirurgicos: [{validaq: true}],

            alergias: [{alergia: ''}],
            alergiasd: [{descripcion: ''}],
            alergiassw: 'No',
            validaalergias: [{validaa: true}],

            transfusionesano: [{ano: 1900}],
            transfusionesdes: [{descripcion: ''}],
            transfusionessw: 'No',
            validatransfusiones: [{validat: true}],

            hipertensionf: false,
            hipertensionffam: 'Sin asignar',
            validahipertensionffam: true,

            tbcpulmonarf: false,
            tbcpulmonarffam: 'Sin asignar',
            validatbcpulmonarf: true,

            diabetesf: false,
            diabetesffam: 'Sin asignar',
            validadiabetesf: true,

            pmamariaf: false,
            pmamariaffam: 'Sin asignar',
            validapmamariaf: true,

            cardiopatiasf: false,
            cardiopatiasffam: 'Sin asignar',
            validacardiopatiasf: true,

            etsf: false,
            etsffam: 'Sin asignar',
            validaetsf: true,

            varicesf: false,
            varicesffam: 'Sin asignar',
            validavaricesf: true,

            erenalf: false,
            erenalffam: 'Sin asignar',
            validaerenalf: true,

            hepatopatiasf: false,
            hepatopatiasffam: 'Sin asignar',
            validahepatopatiasf: true,

            tgenitalesf: false,
            tgenitalesffam: 'Sin asignar',
            validatgenitalesf: true,

            egastrointestinalf: false,
            egastrointestinalffam: 'Sin asignar',
            validaegastrointestinalf: true,

            otrosfamiliarsw: false,
            otrosfamiliar: [{familiar: 'Sin asignar'}],
            otrosfamiliarenf: [{enfermedad: ''}],
            otrosfamiliardropdown: [{dropdownotrosf: false}],
            validaotrosfamiliar: [{validaf: true}],

            observacionesf: '',
            gesta: '',

            menarca: '',
            validamenarca: true,
            
            para: '',
            fum: '',
            abortos: '',
            catamenio: '',
            irs: '',
            planificacionf: 'No',

            metodo: '',
            validametodo: true,

            continua: '',
            cabandono: '',
            gconciencia: 'Normal y sin alteraciones',
            validagconciencia: true,

            pielfaneras: 'Piel de coloración racial con trofismo temperatura y sensibilidad normal, húmeda turgente y elástica. Faneras consevadas',
            validapielfaneras: true,

            caracuello: 'Cráneo normo céfalo, sin protrusiones o depresiones palpables. Ojos: pupilas isocóricas y fotoreactivas. Nariz: Pirámide nasal central y recta, fosas nasales permeables. Oidos: Pabellones auriculares normo implantados con CAE permeables. Cavidad oral y faringe: con mucosa de coloración rosada, faringe de aspecto rosado. Cuello cilíndrico. Pulso carotideo palpable, bilateral. Arcos de movimiento conservados. No se aprecia ingurgitación yugular, tiroides no palpable',
            validacaracuello: true,

            visualauditivo: 'Párpados y globos oculares moviles y de estructura conservada',
            validavisualauditivo: true,

            tiroides: 'No visible ni palpable',
            validatiroides: true,

            cardiopulmonar: 'Regular, igual con dureza, amplitud y celeridad normal',
            validacardiopulmonar: true,

            abdomen: 'Insepección: plano. Auscultación: RHA (+) normoactivos, Palpación: Blando, depresible, no doloroso a la palpación superficial ni profunda, signo de Murphy (-) sin irritación peritoneal al momento',
            validaabdomen: true,

            genitales: 'Puntos ureterales superiores y medios (-) Puño percusión renal bilateral no doloroso',
            validagenitales: true,

            extremidades: 'Miembros superiores y miembros inferiores con tono y trofismo conservado bilateral, no se observan edemas en miembros inferiores, no se evidencian trayectos varicosos, Homans y Ollow negativo',
            validaextremidades: true,

            neurologico: 'Funciones cerebrales superiores: consciente, vigil, lenguaje comprensible, responde conversación, praxia conservada, gnosia conservada, memorias conservada, Pares craneales, sin particularidades, Sistema motor: conservado. Sistema sensitivo: conservado, Sistema vestíbulo cerebeloso: conservado, Marcha, simétrica No signos de irritación meníngea, no signos de focalización',
            validaneurologico: true,

            id_signosvitales: 'sv'+this.props.paciente.dip+f,
            unidadpeso: 'kg.',
            unidadtalla: 'mts.',
            unidadtemperatura: 'Cº',
            unidadpulso: 'lpm',
            unidadpa: 'pa',

            pa: '',
            validapa: true,

            peso: '',
            validapeso: true,

            pulso: '',
            validapulso: true,

            talla: '',
            validatalla: true,

            temperatura: '',
            validatemperatura: true,

            id_examenfisico: 'ef'+this.props.paciente.dip,
            antecedentescvector: [],
            antecedentesfvector: [],
            dropdownOpen: new Array(11).fill(false),
            dropdownHabitos: new Array(2).fill(false),
            cie10: cie10,

            modal: false,
            swloader: false,

            modalerror: false,
        }
        this.cambiaEstado = this.cambiaEstado.bind(this);
        this.enviarInformacionPromise = this.enviarInformacionPromise.bind(this);
        this.cambiaEstadoCheckBox = this.cambiaEstadoCheckBox.bind(this);
        this.cambiaEstadoCheckBoxOtros = this.cambiaEstadoCheckBoxOtros.bind(this);
        this.adicionaCheckBox = this.adicionaCheckBox.bind(this);
        this.cambiaEstadoHabitos = this.cambiaEstadoHabitos.bind(this);
        this.cambiaEstadoCheckBoxOtrosFamiliar = this.cambiaEstadoCheckBoxOtrosFamiliar.bind(this);
        
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModalError = this.toggleModalError.bind(this);

        this.validaHistorial = this.validaHistorial.bind(this);
    }

    enviarInformacionPromise(e){
        console.log('Trabajando');
        e.preventDefault();
        
        this.setState({
            swloader: true
        })

        let alcohol='';
        if(this.state.alcohol == "Consume"){
            alcohol="Consume " + this.state.acantidad +' '+ this.state.acantidadlabel +' '+ this.state.afrecuencia;
        }else
            alcohol="No consume";

        let tabaco='';
        if(this.state.tabaco == "Consume"){
            tabaco="Consume " + this.state.tcantidad +' '+ this.state.tcantidadlabel +' '+ this.state.tfrecuencia;
        }else
            tabaco="No consume";

        let combe='';
        if(this.state.combe)
            combe="Convive con una persona con "+this.state.combedesc;
        else
            combe="No";

        let otros;
        if(this.state.hipertension)
            this.state.antecedentescvector.push('Hipertension');
        if(this.state.tbcpulmonar)
            this.state.antecedentescvector.push('TBC Pulmonar');
        if(this.state.diabetes)
            this.state.antecedentescvector.push('Diabetes');
        if(this.state.pmamaria)
            this.state.antecedentescvector.push('P. Mamaria');
        if(this.state.cardiopatias)
            this.state.antecedentescvector.push('Cardiopatias');
        if(this.state.ets)
            this.state.antecedentescvector.push('Enfermedad de Transmision sexual');
        if(this.state.varices)
            this.state.antecedentescvector.push('Varices');
        if(this.state.erenal)
            this.state.antecedentescvector.push('Enfermedad Renal');
        if(this.state.hepatopatias)
            this.state.antecedentescvector.push('Hepatopatias');
        if(this.state.tgenitales)
            this.state.antecedentescvector.push('Tumores Genitales');
        if(this.state.egastrointestinal)
            this.state.antecedentescvector.push('Enfermedad Gastrointestinal');
        if(this.state.otrossw){
            this.state.otros.map((item) => {
                this.state.antecedentescvector.push(item.enfermedad);
                return 0;
            });
        }

        if(this.state.aquirurgicossw){

        }


        // Para los familiares 
        let objetoaf={}
        if(this.state.hipertensionf){
            objetoaf={
                antecedentef: 'Hipertension',
                familiar: this.state.hipertensionffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.tbcpulmonarf){
            objetoaf={
                antecedentef: 'TBC Pulmonar',
                familiar: this.state.tbcpulmonarffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.diabetesf){
            objetoaf={
                antecedentef: 'Diabetes',
                familiar: this.state.diabetesffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.pmamariaf){
            objetoaf={
                antecedentef: 'P. Mamaria',
                familiar: this.state.pmamariaffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.cardiopatiasf){
            objetoaf={
                antecedentef: 'Cardiopatias',
                familiar: this.state.cardiopatiasffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.etsf){
            objetoaf={
                antecedentef: 'Enfermedad de Transmision sexual',
                familiar: this.state.etsffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.varicesf){
            objetoaf={
                antecedentef: 'Varices',
                familiar: this.state.varicesffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.erenalf){
            objetoaf={
                antecedentef: 'Enfermedad Renal',
                familiar: this.state.erenalffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.hepatopatiasf){
            objetoaf={
                antecedentef: 'Hepatopatias',
                familiar: this.state.hepatopatiasffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.tgenitalesf){
            objetoaf={
                antecedentef: 'Tumores Genitales',
                familiar: this.state.tgenitalesffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.egastrointestinalf){
            objetoaf={
                antecedentef: 'Enfermedad Gastrointestinal',
                familiar: this.state.egastrointestinalffam
            }
            this.state.antecedentesfvector.push(objetoaf);
        }
        if(this.state.otrosfamiliarsw){
            let objetoafotros={}
            this.state.otrosfamiliar.map((item, i) => {
                objetoafotros={
                    antecedentef: this.state.otrosfamiliarenf[i].enfermedad,
                    familiar: item.familiar
                }
                this.state.antecedentesfvector.push(objetoafotros);
                return 0;
            })
        }
        
        
        // Historia Clínica
        let urlhc=''
        let datahc={}

        urlhc='http://localhost:8000/api/v1/historiaclinica/'
        datahc={
            id_historiaclinica: this.state.id_historiac,
            fecha_primeraconsulta: this.state.fecha,
            hora_primeraconsulta: this.state.hora.toLocaleTimeString(),
            afiliado: this.props.paciente.dip,
            motivo_consulta: this.state.consulta,
            historia_enfermedad_actual: this.state.historia,
            datos_complementarios: this.state.datosc,
            diagnostico_presuntivo: this.state.diagnostico,
        }
        try{
            fetch(urlhc, {
                method: 'POST',
                body: JSON.stringify(datahc),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('HistoriaClinica:', response))
            .then(() =>{
                //Examen Fisico
                let urlef=''
                let dataef={}

                urlef='http://localhost:8000/api/v1/examenfisico/'
                dataef={
                    id_examenfisico: this.state.id_examenfisico,
                    estado_general: this.state.gconciencia,
                    piel_faneras: this.state.pielfaneras,
                    cara_cuello: this.state.caracuello,
                    visual_auditivo: this.state.visualauditivo,
                    tiroides: this.state.tiroides,
                    cardiopulmonar: this.state.cardiopulmonar,
                    abdomen: this.state.abdomen,
                    genitales: this.state.genitales,
                    extremidades: this.state.extremidades,
                    neurologico: this.state.neurologico,
                    historia_clinica: this.state.id_historiac
                }
                try{
                    fetch(urlef,{
                        method: 'POST',
                        body: JSON.stringify(dataef),
                        headers:{
                            'Content-Type': 'application/json'
                        }
                    }).then(res => res.json())
                    .catch(error => console.error('Error',error))
                    .then(respuesta => console.log('ExamenFisico:',respuesta))
                    .then(() => {
                        //SignosVitales
                        let urlsv=''
                        let datasv={}

                        urlsv='http://localhost:8000/api/v1/signosvitales/'
                        datasv={
                            id_signosvitales: this.state.id_signosvitales + this.state.hora.toLocaleTimeString(),
                            unidad_peso: this.state.unidadpeso,
                            unidad_talla: this.state.unidadtalla,
                            unidad_temperatura: this.state.unidadtemperatura,
                            unidad_pulso: this.state.unidadpulso,
                            unidad_PA: this.state.unidadpa,
                            talla: this.state.talla,
                            peso: this.state.peso,
                            temperatura: this.state.temperatura,
                            pulso: this.state.pulso,
                            pa: this.state.pa,
                            examen_fisico: this.state.id_examenfisico
                        }
                        try{
                            fetch(urlsv,{
                                method: 'POST',
                                body: JSON.stringify(datasv),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.error('Error:', error))
                            .then(response => console.log('SignosVitales:',response));
                        }catch(e){
                            console.log(e);
                        }
                    });
                }catch(e){
                    console.log(e);
                }

                //Anamnesis

                let urlan=''
                let dataan={}
        
                urlan='http://localhost:8000/api/v1/anamnesis/'
                dataan={
                    id_anamnesis: this.state.id_anamnesis,
                    fecha_actualizacion: this.state.fecha_actualizacion,
                    historia_clinica: this.state.id_historiac
                }
                try{
                    fetch(urlan, {
                        method: 'POST',
                        body: JSON.stringify(dataan),
                        headers:{
                            'Content-Type': 'application/json'
                        }
                    }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Anamnesis:', response))
                    .then(() =>{
                        // Antecedentes Personales no patologicos

                        let urlapnp=''
                        let dataapnp={}

                        urlapnp='http://localhost:8000/api/v1/antecedentespnp/'
                        dataapnp={
                            vivienda: this.state.vivienda,
                            servicio_higienico_intradomiciliario: this.state.shi,
                            agua_intradomiciliaria: this.state.aguai,
                            alimentacion: this.state.alimentacion,
                            habito_tabaquico: tabaco,
                            habito_alcoholico: alcohol,
                            somnia: this.state.somnia + ' horas al dia',
                            diuresis: this.state.diuresis + ' veces al dia',
                            habito_intestinal: this.state.hintestinal + ' veces al dia',
                            anamnesis: this.state.id_anamnesis,
                        }
                        try{
                            fetch(urlapnp, {
                                method: 'POST',
                                body: JSON.stringify(dataapnp),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.log('Error:', error))
                            .then(response => console.log('APNP:', response));
                        }catch(e){
                            console.log(e)
                        }

                        // Antecedentes Familiares
                        let urlaf=''
                        urlaf='http://localhost:8000/api/v1/antecedentesf/'
                        let dataaf={}
                        this.state.antecedentesfvector.map((item) => {
                            dataaf={
                                antecedentef: item.antecedentef,
                                familiar: item.familiar,
                                anamnesis: this.state.id_anamnesis
                            }
                            try{
                                fetch(urlaf,{
                                    method: 'POST',
                                    body: JSON.stringify(dataaf),
                                    headers:{
                                        'Content-Type': 'application/json'
                                    }
                                }).then(res => res.json())
                                .catch(error => console.error('Error:', error))
                                .then(response => console.log('AFamiliares:', response));
                            }catch(e){
                                console.log(e);
                            }
                            return 0;
                        })


                        // Antecedentes Gineco Obstetricos
                        if(this.props.paciente.id_sexo=='F'){
                            let urlago=''
                            let dataago={}
                            
                            urlago='http://localhost:8000/api/v1/antecedentesgo/'
                            dataago={
                                gesta: this.state.gesta,
                                menarca: this.state.menarca,
                                para: this.state.para,
                                fum: this.state.fum,
                                abortos: this.state.abortos,
                                catamenio: this.state.catamenio,
                                irs: this.state.irs,
                                anamnesis: this.state.id_anamnesis
                            }
                            try{
                                fetch(urlago,{
                                    method: 'POST',
                                    body: JSON.stringify(dataago),
                                    headers:{
                                        'Content-Type': 'application/json'
                                    }
                                }).then(res => res.json())
                                .catch(error => console.error('Error:',error))
                                .then(response => console.log('AntecedentesGO:',response));
                            }catch(e){
                                console.log(e);
                            }
                        }


                        //Planificacion Familiar
                        if(this.state.planificacionf == 'Si'){
                            let urlpf=''
                            let datapf={}
                            
                            urlpf='http://localhost:8000/api/v1/planificacionf/'
                            datapf={
                                planificacion: this.state.planificacionf,
                                metodo: this.state.metodo,
                                continua: this.state.continua,
                                causa_abandono: this.state.cabandono,
                                anamnesis: this.state.id_anamnesis
                            }
                            try{
                                fetch(urlpf,{
                                    method: 'POST',
                                    body: JSON.stringify(datapf),
                                    headers:{
                                        'Content-Type': 'application/json'
                                    }
                                }).then(res => res.json())
                                .catch(error => console.error('Error:', error))
                                .then(response => console.log('PFamiliar:', response))
                            }catch(e){
                                console.log(e);
                            }
                        }


                        //Antecedentes Personales Patologicos
                        let urlapp=''
                        let dataapp={}

                        urlapp='http://localhost:8000/api/v1/antecedentespp/'
                        dataapp={
                            id_app: this.state.id_app,
                            combe: combe,
                            anamnesis: this.state.id_anamnesis,
                        }
                        try{
                            fetch(urlapp, {
                                method: 'POST',
                                body: JSON.stringify(dataapp),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.error('Error:', error))
                            .then(response => console.log('APP:', response)).
                            then(() => {
                                // Antecedentes Clinicos
                                this.state.antecedentescvector.map((item) => {
                                    let urlac=''
                                    let dataac={}
                                    urlac='http://localhost:8000/api/v1/antecedentesc/'
                                    dataac={
                                        antecedentec: item,
                                        app: this.state.id_app
                                    }
                                    try{
                                        fetch(urlac,{
                                            method: 'POST',
                                            body: JSON.stringify(dataac),
                                            headers:{
                                                'Content-Type': 'application/json'
                                            }
                                        }).then(res => res.json())
                                        .catch(error => console.error('Error:', error))
                                        .then(response => console.log('AClinicos:', response));
                                    }catch(e){
                                        console.log(e)
                                    }
                                    return 0;
                                })
                                

                                // Antecedentes Quirurgicos
                                if(this.state.aquirurgicossw == 'Si'){
                                    let urlaq = ''
                                    let dataaq = {}

                                    urlaq='http://localhost:8000/api/v1/antecedentesq/'
                                    this.state.aquirurgicos.map((item, idx) => {
                                        dataaq={
                                            ano: this.state.aquirurgicosano[idx].ano,
                                            tratamiento_quirurgico: item.tratamientoq,
                                            app: this.state.id_app
                                        }

                                        try{
                                            fetch(urlaq, {
                                                method: 'POST',
                                                body: JSON.stringify(dataaq),
                                                headers:{
                                                    'Content-Type': 'application/json'
                                                }
                                            }).then(res => res.json())
                                            .catch(error => console.error('Error:', error))
                                            .then(response => console.log('AQuirurgicos:', response));
                                        }catch(e){
                                            console.log(e);
                                        }
                                        return 0;
                                    });
                                }


                                // Alergias
                                if(this.state.alergiassw == 'Si'){
                                    let urlal=''
                                    let dataal={}
                                    
                                    urlal='http://localhost:8000/api/v1/alergias/'
                                    this.state.alergias.map((item) => {
                                        dataal={
                                            alergia: item.alergia,
                                            app: this.state.id_app
                                        }
                                        try{
                                            fetch(urlal,{
                                                method: 'POST',
                                                body: JSON.stringify(dataal),
                                                headers:{
                                                    'Content-Type': 'application/json'
                                                }
                                            }).then(res => res.json())
                                            .catch(error => console.error('Error:', error))
                                            .then(response => console.log('Alergias:', response))
                                        }catch(e){
                                            console.log(e);
                                        }
                                        return 0;
                                    });
                                    
                                }

                                    
                                //Transfusiones
                                if(this.state.transfusionessw == 'Si'){
                                    let urltr=''
                                    let datatr={}
                                    urltr='http://localhost:8000/api/v1/transfusiones/'
                                    this.state.transfusionesano.map((item, idx) => {
                                        datatr={
                                            ano: item.ano,
                                            descripcion: this.state.transfusionesdes[idx].descripcion,
                                            app: this.state.id_app
                                        }
                                        try{
                                            fetch(urltr,{
                                            method: 'POST',
                                            body: JSON.stringify(datatr),
                                            headers:{
                                                'Content-Type': 'application/json'
                                            }
                                        }).then(res => res.json())
                                        .catch(error => console.error('Error', error))
                                        .then(response => console.log('Transfusiones', response));
                                        }catch(e){
                                            console.log(e);
                                        }
                                        return 0;
                                    });
                                }
                            })
                            .then(() => {
                                this.setState({
                                    modal: false,
                                    swloader: false
                                })
                                toast.success("Historia Clinica Guardada");
                            });
                        }catch(e){
                            console.log(e)
                        }
                    });
                }catch(e){
                    console.log(e)
                } 
            });
        }catch(e){
            console.log(e)
        }

        this.setState({
            hora: new Date()
        });
    }

    validaHistorial(e){
        e.preventDefault();
        let error = false;
        if(this.state.consulta.length === 0 || !this.state.consulta.trim()){
            error=true;
            this.setState({
                validaconsulta: false,
            })
        }
        if(this.state.historia.length === 0 || !this.state.historia.trim()){
            error=true;
            this.setState({
                validahistoria: false,
            })
        }
        if(this.state.alimentacion.length === 0 || !this.state.alimentacion.trim()){
            error=true;
            this.setState({
                validaalimentacion: false,
            })
        }
        if(this.state.alcohol === 'Consume'){
            if(this.state.acantidad < 1 || !Number.isInteger(parseFloat(this.state.acantidad)) || this.state.afrecuencia === 'al dia, semana, año...'){
                error=true;
                this.setState({
                    validaalcohol: false,
                })
            }
        }
        if(this.state.tabaco === 'Consume'){
            if(this.state.tcantidad < 1 || !Number.isInteger(parseFloat(this.state.tcantidad)) || this.state.tfrecuencia === 'al dia, semana, año...'){
                error=true;
                this.setState({
                    validatabaco: false,
                })
            }
        }
        if(this.state.somnia < 1 || !Number.isInteger(parseFloat(this.state.somnia))){
            error=true;
            this.setState({
                validasomnia: false,
            })
        }
        if(this.state.diuresis < 1 || !Number.isInteger(parseFloat(this.state.diuresis))){
            error=true;
            this.setState({
                validadiuresis: false,
            })
        }
        if(this.state.hintestinal < 1 || !Number.isInteger(parseFloat(this.state.hintestinal))){
            error=true;
            this.setState({
                validahintestinal: false,
            })
        }
        if(this.state.combe){
            if(this.state.combedesc.length === 0 || !this.state.combedesc.trim()){
                error=true;
                this.setState({
                    validacombe: false,
                })
            }
        }
        if(this.state.otrossw){
            const newValidaOtros = this.state.otros.map((item, i) => {
                if(item.enfermedad.length === 0 || !item.enfermedad.trim()){
                    error=true;
                    return {valida: false}
                }
                return {valida: true}
            });
            this.setState({
                validaotros: newValidaOtros,
            });
        }
        if(this.state.aquirurgicossw == 'Si'){
            const newValidaAQuirurgicos = this.state.aquirurgicos.map((item, i) => {
                if(item.tratamientoq.length === 0 || !item.tratamientoq.trim() || this.state.aquirurgicosano[i].ano < 1900 || !Number.isInteger(parseFloat(this.state.aquirurgicosano[i].ano))){
                    error=true;
                    return {validaq: false}
                }
                return {validaq: true}
            });
            this.setState({
                validaaquirurgicos: newValidaAQuirurgicos,
            });
        }
        if(this.state.alergiassw == 'Si'){
            const newValidaAlergias = this.state.alergias.map((item) => {
                if(item.alergia.length === 0 || !item.alergia.trim()){
                    error=true;
                    return {validaa: false}
                }
                return {validaa: true}
            });
            this.setState({
                validaalergias: newValidaAlergias,
            });
        }
        if(this.state.transfusionessw == 'Si'){
            const newValidaTransfusiones = this.state.transfusionesano.map((item, i) => {
                if(item.ano < 1900 || !Number.isInteger(parseFloat(item.ano)) || this.state.transfusionesdes[i].descripcion.length === 0 || !this.state.transfusionesdes[i].descripcion.trim()){
                    error=true;
                    return {validat: false}
                }
                return {validat: true}
            });
            this.setState({
                validatransfusiones: newValidaTransfusiones,
            })
        }
        if(this.state.hipertensionf){
            if(this.state.hipertensionffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validahipertensionffam: false,
                })
            }
        }
        if(this.state.tbcpulmonarf){
            if(this.state.tbcpulmonarffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validatbcpulmonarf: false,
                })
            }
        }
        if(this.state.diabetesf){
            if(this.state.diabetesffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validadiabetesf: false,
                })
            }
        }
        if(this.state.pmamariaf){
            if(this.state.pmamariaffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validapmamariaf: false,
                })
            }
        }
        if(this.state.cardiopatiasf){
            if(this.state.cardiopatiasffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validacardiopatiasf: false,
                })
            }
        }
        if(this.state.etsf){
            if(this.state.etsffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validaetsf: false,
                })
            }
        }
        if(this.state.varicesf){
            if(this.state.varicesffam === 'Sin asignar'){
                this.setState({
                    validavaricesf: false,
                    error: true
                })
            }
        }
        if(this.state.erenalf){
            if(this.state.erenalffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validaerenalf: false,
                })
            }
        }
        if(this.state.hepatopatiasf){
            if(this.state.hepatopatiasffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validahepatopatiasf: false,
                })
            }
        }
        if(this.state.tgenitalesf){
            if(this.state.tgenitalesffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validatgenitalesf: false,
                })
            }
        }
        if(this.state.egastrointestinalf){
            if(this.state.egastrointestinalffam === 'Sin asignar'){
                error=true;
                this.setState({
                    validaegastrointestinalf: false,
                })
            }
        }
        if(this.state.otrosfamiliarsw){
            const newValidaOtrosFamiliares = this.state.otrosfamiliar.map((item, i) => {
                if(item.familiar === 'Sin asignar' || this.state.otrosfamiliarenf[i].enfermedad.length === 0 || !this.state.otrosfamiliarenf[i].enfermedad.trim()){
                    error=true;
                    return {validaf: false}
                }
                return {validaf: true}
            });
            this.setState({
                validaotrosfamiliar: newValidaOtrosFamiliares,
            })
        }
        if(this.props.paciente.id_sexo == 'F'){
            if(this.state.menarca < 1 || !Number.isInteger(parseFloat(this.state.menarca))){
                error=true;
                this.setState({
                    validamenarca: false,
                })
            }
        }
        if(this.state.planificacionf === 'Si'){
            if(this.state.metodo.length === 0 || !this.state.metodo.trim()){
                error=true;
                this.setState({
                    validametodo: false,
                })
            }
        }
        if(this.state.pa < 1){
            error=true;
            this.setState({
                validapa: false,
            })
        }
        if(this.state.peso < 1){
            error=true;
            this.setState({
                validapeso: false,
            })
        }
        if(this.state.pulso < 1){
            error=true;
            this.setState({
                validapulso: false,
            })
        }
        if(this.state.talla < 1){
            error=true;
            this.setState({
                validatalla: false,
            })
        }
        if(this.state.temperatura < 1){
            error=true;
            this.setState({
                validatemperatura: false,
            })
        }
        if(this.state.gconciencia.length === 0 || !this.state.gconciencia.trim()){
            error=true;
            this.setState({
                validagconciencia: false,
            })
        }
        if(this.state.pielfaneras.length === 0 || !this.state.pielfaneras.trim()){
            error=true;
            this.setState({
                validapielfaneras: false,
            })
        }
        if(this.state.caracuello.length === 0 || !this.state.caracuello.trim()){
            error=true;
            this.setState({
                validacaracuello: false,
            })
        }
        if(this.state.visualauditivo.length === 0 || !this.state.visualauditivo.trim()){
            error=true;
            this.setState({
                validavisualauditivo: false,
            })
        }
        if(this.state.tiroides.length === 0 || !this.state.tiroides.trim()){
            error=true;
            this.setState({
                validatiroides: false,
            })
        }
        if(this.state.cardiopulmonar.length === 0 || !this.state.cardiopulmonar.trim()){
            error=true;
            this.setState({
                validacardiopulmonar: false,
            })
        }
        if(this.state.abdomen.length === 0 || !this.state.abdomen.trim()){
            error=true;
            this.setState({
                validaabdomen: false,
            })
        }
        if(this.state.genitales.length === 0 || !this.state.genitales.trim()){
            error=true;
            this.setState({
                validagenitales: false,
            })
        }
        if(this.state.extremidades.length === 0 || !this.state.extremidades.trim()){
            error=true;
            this.setState({
                validaextremidades: false,
            })
        }
        if(this.state.neurologico.length === 0 || !this.state.neurologico.trim()){
            error=true;
            this.setState({
                validaneurologico: false,
            })
        }
        if(this.state.diagnostico.length === 0 || !this.state.diagnostico.trim()){
            error=true;
            this.setState({
                validadiagnostico: false,
            })
        }

        // IMPORTANTE!!
        console.log(error);
        if(error === true){
            this.setState({
                modalerror: true,
                modal: false,
            })
        }
        else {
            console.log('Enviando información...');
            this.enviarInformacionPromise(e);
        }
    }

    cambiaEstado(event){
        this.setState({vivienda: event.target.value});
    }

    prueba1 = (e) => {
        e.preventDefault();
        console.log('Otros'+this.state.otrossw);
        this.state.otros.map((item) => {
            console.log(item);
            return 0;
        })
    }
    cambiaEstadoCheckBox(event){
        const target = event.target;
        const value = target.type === 'checkbox' ?
        target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    cambiaEstadoCheckBoxOtros(event){
        const target = event.target;
        const value = target.type === 'checkbox' ?
        target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
        if(value == false){
            this.setState({
                otros: [{enfermedad: ''}],
                validaotros: [{valida: true}]
            });
        }
    }
    cambiaEstadoCheckBoxOtrosFamiliar(event){
        const target = event.target;
        const value = target.type === 'checkbox' ?
        target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
        if(value == false){
            this.setState({
                otrosfamiliar: [{familiar: ''}],
                otrosfamiliarenf: [{enfermedad: ''}],
                validaotrosfamiliar: [{validaf: true}],
            });
        }
    }
    adicionaCheckBox(event){
        const target = event.target;
        const value = target.type === 'checkbox' ?
        target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        })

        if(value == true)
            this.state.antecedentescvector.push(name);
    }

    toggle(i){
        const newArray = this.state.dropdownOpen.map((element, index) => {
            return (index === i ? !element: false)
        });
        if(i === 0) this.setState({validahipertensionffam: true});
        if(i === 1) this.setState({validatbcpulmonarf: true});
        if(i === 2) this.setState({validadiabetesf: true});
        if(i === 3) this.setState({validapmamariaf: true});
        if(i === 4) this.setState({validacardiopatiasf: true});
        if(i === 5) this.setState({validaetsf: true});
        if(i === 6) this.setState({validavaricesf: true});
        if(i === 7) this.setState({validaerenalf: true});
        if(i === 8) this.setState({validahepatopatiasf: true});
        if(i === 9) this.setState({validatgenitalesf: true});
        if(i === 10) this.setState({validaegastrointestinalf: true});
        this.setState({
            dropdownOpen: newArray,
        });
    }
    toggleHabitos(i){
        const newArray = this.state.dropdownHabitos.map((element, index) => {
            return (index ===i ? !element: false)
        });
        if(i === 0) this.setState({validaalcohol: true});
        if(i === 1) this.setState({validatabaco: true});
        this.setState({
            dropdownHabitos: newArray
        })
    }

    adicionaOtrosc = () =>{
        this.setState({
            otros: this.state.otros.concat([{enfermedad: ''}]),
            validaotros: this.state.validaotros.concat([{valida: true}]),
        });
    }
    eliminaOtrosc = (idx) => () =>{
        this.setState({
            otros: this.state.otros.filter((s, sidx) => idx !== sidx),
            validaotros: this.state.validaotros.filter((s, sidx) => idx !== sidx)
        });
    }
    adicionaOtrosSetState = (idx) => (evt) => {
        const newOtrosc = this.state.otros.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {enfermedad: evt.target.value}
        });
        const newValidaOtros = this.state.validaotros.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {valida: true}
        });
        this.setState({ otros: newOtrosc,  validaotros: newValidaOtros});
    }
    cambiaEstadoHabitos(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
        if(name === 'acantidad') this.setState({validaalcohol: true});
        if(name === 'tcantidad') this.setState({validatabaco: true});
        const namelabel = name+'label';
        if(value > 1)
            this.setState({[namelabel]: 'veces'});
        else
            this.setState({[namelabel]: 'vez'});
    }

    // Aquirurgicos
    adicionaAQuirurgicos = () => {
        this.setState({
            aquirurgicos: this.state.aquirurgicos.concat([{tratamientoq: ''}]),
            aquirurgicosano: this.state.aquirurgicosano.concat([{ano: 1900}]),
            validaaquirurgicos: this.state.validaaquirurgicos.concat([{validaq: true}])
        });
    }
    eliminaAQuirurgicos = (idx) => () =>{
        this.setState({
            aquirurgicos: this.state.aquirurgicos.filter((s, sidx) => idx !== sidx),
            aquirurgicosano: this.state.aquirurgicosano.filter((s, sidx) => idx !== sidx),
            validaaquirurgicos: this.state.validaaquirurgicos.filter((s, sidx) => idx !== sidx)
        });
    }
    adicionaAQuirurgicosSetState = (idx) => (evt) => {
        const newAquirurgicos = this.state.aquirurgicos.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {tratamientoq: evt.target.value}
        });
        const newValidaAQuirurgicos = this.state.validaaquirurgicos.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {validaq: true}
        });
        this.setState({aquirurgicos: newAquirurgicos, validaaquirurgicos: newValidaAQuirurgicos})
    }
    adicionaAQuirurgicosAnoSetState = (idx) => (evt) => {
        const newAquirurgicosAno = this.state.aquirurgicosano.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {ano: evt.target.value}
        });
        const newValidaAQuirurgicos = this.state.validaaquirurgicos.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {validaq: true}
        });
        this.setState({aquirurgicosano: newAquirurgicosAno, validaaquirurgicos: newValidaAQuirurgicos})
    }

    // Alergias
    adicionaAlergia = () => {
        this.setState({
            alergias: this.state.alergias.concat([{alergia: ''}]),
            alergiasd: this.state.alergiasd.concat([{descripcion: ''}]),
            validaalergias: this.state.validaalergias.concat([{validaa: true}])
        })
    }
    eliminaAlergia = (idx) => () =>{
        this.setState({
            alergias: this.state.alergias.filter((s, sidx) => idx !== sidx),
            alergiasd: this.state.alergiasd.filter((s, sidx) => idx !== sidx),
            validaalergias: this.state.validaalergias.filter((s, sidx) => idx !== sidx)
        });
    }
    adicionaAlergiaSetState = (idx) => (evt) => {
        const newAlergia = this.state.alergias.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {alergia: evt.target.value}
        });
        const newValidaAlergias = this.state.validaalergias.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {validaa: true}
        })
        this.setState({alergias: newAlergia, validaalergias: newValidaAlergias})
    }
    adicionaAlergiaDescripcionSetState = (idx) => (evt) => {
        const newAlergiaD = this.state.alergiasd.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {descripcion: evt.target.value}
        })
        this.setState({alergiasd: newAlergiaD})
    }

    // Transfusiones
    adicionaTransfusiones = () => {
        this.setState({
            transfusionesano: this.state.transfusionesano.concat([{ano: 1900}]),
            transfusionesdes: this.state.transfusionesdes.concat([{descripcion: ''}]),
            validatransfusiones: this.state.validatransfusiones.concat([{validat: true}])
        })
    }
    eliminaTransfusiones = (idx) => () => {
        this.setState({
            transfusionesano: this.state.transfusionesano.filter((s, sidx) => idx !== sidx),
            transfusionesdes: this.state.transfusionesdes.filter((s, sidx) => idx !== sidx),
            validatransfusiones: this.state.validatransfusiones.filter((s, sidx) => idx !== sidx)
        })
    }
    adicionaTransfusionesSetState = (idx) => (evt) => {
        const newTransfusion = this.state.transfusionesano.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {ano: evt.target.value}
        });
        const newValidaTransfusiones = this.state.validatransfusiones.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {validat: true}
        });
        this.setState({transfusionesano: newTransfusion, validatransfusiones: newValidaTransfusiones})
    }
    adicionaTransfusionesDescripcionSetState = (idx) => (evt) => {
        const newTransfusionDes = this.state.transfusionesdes.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {descripcion: evt.target.value}
        });
        const newValidaTransfusiones = this.state.validatransfusiones.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {validat: true}
        });
        this.setState({transfusionesdes: newTransfusionDes, validatransfusiones: newValidaTransfusiones})
    }

    // OTRAS enfermedades familiares
    adicionaOtrosFamiliar = () => {
        this.setState({
            otrosfamiliar: this.state.otrosfamiliar.concat([{familiar: 'Sin asignar'}]),
            otrosfamiliarenf: this.state.otrosfamiliarenf.concat([{enfermedad: ''}]),
            otrosfamiliardropdown: this.state.otrosfamiliardropdown.concat([{dropdownotrosf: false}]),
            validaotrosfamiliar: this.state.validaotrosfamiliar.concat([{validaf: true}])
        });
    }
    eliminaOtrosFamiliar = (idx) => () =>{
        this.setState({
            otrosfamiliar: this.state.otrosfamiliar.filter((s, sidx) => idx !== sidx),
            otrosfamiliarenf: this.state.otrosfamiliarenf.filter((s, sidx) => idx !== sidx),
            otrosfamiliardropdown: this.state.otrosfamiliardropdown.filter((s, sidx) => idx !== sidx),
            validaotrosfamiliar: this.state.validaotrosfamiliar.filter((s, sidx) => idx !== sidx),
        });
    }
    adicionaOtrosFamiliarEnfermedadSetState = (idx) => (evt) => {
        const newOtrosFamiliarEnfermedad = this.state.otrosfamiliarenf.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {enfermedad: evt.target.value}
        });
        const newValidaOtrosFamiliar = this.state.validaotrosfamiliar.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {validaf: true}
        });
        this.setState({otrosfamiliarenf: newOtrosFamiliarEnfermedad, validaotrosfamiliar: newValidaOtrosFamiliar})
    }
    adicionaOtrosFamiliarSetState = (idx) => (evt) => {
        const newOtrosFamiliar = this.state.otrosfamiliar.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {familiar: evt.target.value}
        });
        this.setState({otrosfamiliar: newOtrosFamiliar})
    }
    toggleOtrosFamiliar(i){
        const newArray = this.state.otrosfamiliardropdown.map((elemento, index) => {
            return (index === i ? {dropdownotrosf: !elemento.dropdownotrosf}: {dropdownotrosf: false})
        })
        const newValidaOtrosFamiliar = this.state.validaotrosfamiliar.map((item, sidx) => {
            if(sidx !== i) return item;
            return {validaf: true}
        }); 
        this.setState({
            otrosfamiliardropdown: newArray,
            validaotrosfamiliar: newValidaOtrosFamiliar
        })
    }
    
    cie10(value){
        // console.log(value)
        this.setState({ diagnostico: value, validadiagnostico: true })
    }

    toggleModal = (item) => () => {
        this.setState({
          modal: !this.state.modal
        })
      }
    
    toggleModalError(e){
        e.preventDefault();
        this.setState({
            modalerror: !this.state.modalerror
        })
    }
    render() {
        return (
          <div className="animated fadeIn" class="historiaclinica">
            <Row>
                <Col md="12">
                <Card>
                    <CardBody>
                    <Encabezado />
                    <div class="thistoria"><h2><b>HISTORIA CLÍNICA</b></h2></div>
                    <div class="fhistoria"><h5><span>Fecha de consulta: {this.state.fecha} </span></h5></div>
                    <div class="prueba-hr">
                        <h4 class="seccion">Datos Personales</h4>
                        <hr class="hr-dinamico"/>
                    </div>
                    <Table responsive borderless size="sm">
                        <tbody class>
                        <tr>
                            <td><b>Nombre: </b>{this.props.paciente.primer_apellido + ' ' + this.props.paciente.segundo_apellido + ' ' + this.props.paciente.nombres}</td>
                            <td><b>Matrícula: </b>{this.props.paciente.matricula}</td>
                        </tr>
                        <tr>
                            <td><b>Sexo: </b>{this.props.paciente.id_sexo}</td>
                            <td><b>Edad: </b>{this.props.paciente.fec_nacimiento}</td>
                        </tr>
                        <tr>
                            <td><b>Procedencia: </b>{this.props.paciente.sede}</td>
                            <td><b>Residencia: </b>{this.props.paciente.sede}</td>
                        </tr>
                        <tr>
                            <td><b>Estado civil: </b>{this.props.paciente.estado_civil}</td>
                            <td><b>Domicilio: </b>{this.props.paciente.direccion}</td>
                        </tr>
                        {/* <tr>
                            <td>{this.props.afiliadotelefono.length > 1 ? <b>Teléfonos: </b> : <span>{this.props.afiliadotelefono.length !== 0 ? <b>Teléfono: </b>: <span><b>Teléfono: </b>No refiere</span>}</span>}
                                {this.props.afiliadotelefono.map((item, idx, vec) => (
                                    <span>{item.telefono}{idx < vec.length - 1 ? <span>, </span>: <span></span>}</span>
                                ))}
                            </td>
                        </tr> */}
                        </tbody>
                    </Table> 

                    <div class="prueba-hr">
                        <h4 class="seccion">Familiares de referencia</h4>
                        <hr class="hr-dinamico"/>
                    </div>

                    <FormGroup row>
                        <Col md="4">
                            <Label for="papellidof">Primer Apellido</Label>
                            <Input
                                size="lg"
                                type="text"
                                id="papellidof"
                            />
                        </Col>
                        <Col md="4">
                            <Label for="sapellidof">Segundo Apellido</Label>
                            <Input
                                size="lg"
                                type="text"
                                id="sapellidof"
                            />
                        </Col>
                        <Col md="4">
                            <Label for="nombresf">Nombres</Label>
                            <Input
                                size="lg"
                                type="text"
                                id="nombresf"
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="8">
                            <Label for="direccionf">Dirección</Label>
                            <Input 
                                size="lg"
                                type="text"
                                id="direccionf"
                            />
                        </Col>
                        <Col md="4">
                            <Label for="telefonof">Teléfonos</Label>
                            <InputGroup size="lg">
                                <Input
                                    size="lg"
                                    type="number"
                                    id="telefonof"
                                />
                                <InputGroupAddon addonType="append">
                                    <Button color="success">+</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </Col>
                    </FormGroup>
                    

                    

                    
                    
                    <h2 class="tseccion">ANAMNESIS</h2>
                    
                    <div class="prueba-hr">
                        <h4 class="seccion">Antecedentes Personales no patológicos</h4>
                        <hr class="hr-dinamico"/>
                    </div>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Vivienda</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios1" value="Propia" checked={this.state.vivienda==='Propia'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio1">Propia</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios1" value="Alquilada" checked={this.state.vivienda==='Alquilada'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio2">Alquilada</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio3" name="inline-radios1" value="Cedida" checked={this.state.vivienda==='Cedida'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio3">Cedida</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio4" name="inline-radios1" value="Anticretico" checked={this.state.vivienda==='Anticretico'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio4">Anticrético</Label>
                            </FormGroup>
                        </Col>
                    </FormGroup>
                    
                    <FormGroup row>
                        <Col md="4">
                            <Label>Servicio Higienico Intradomiciliario</Label>
                        </Col>
                        <Col md="2">
                            <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio11" name="inline-radios2" value="Si" checked={this.state.shi==='Si'} onChange={e => this.setState({shi: e.target.value})} />
                                <Label check className="form-check-label" htmlFor="inline-radio11">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio12" name="inline-radios2" value="No" checked={this.state.shi==='No'} onChange={e => this.setState({shi: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio12">No</Label>
                            </FormGroup>
                        </Col>

                        <Col md="4">
                            <Label>Agua Intradomiciliaria</Label>
                        </Col>
                        <Col md="2">
                        <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio21" name="inline-radios3" value="Si" checked={this.state.aguai==='Si'} onChange={e => this.setState({aguai: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio21">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio22" name="inline-radios3" value="No" checked={this.state.aguai==='No'} onChange={e => this.setState({aguai: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio22">No</Label>
                            </FormGroup>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="2">
                            <Label>Alimentación: </Label>
                        </Col>
                        <Col xs="12" md="10">
                            <InputGroup size="lg">
                                
                                <Input 
                                    size="lg"
                                    type="text"
                                    value={this.state.alimentacion}
                                    onChange={e => this.setState({alimentacion: e.target.value, validaalimentacion: true})}
                                />
                                {this.state.alimentacion != '' &&
                                    <InputGroupAddon addonType="append"><Button color="danger" onClick={e => this.setState({alimentacionrespaldo: this.state.alimentacion, alimentacion: ''})}>limpiar</Button></InputGroupAddon>
                                }
                                {this.state.alimentacion == '' &&
                                    <InputGroupAddon addonType="append"><Button color="warning" onClick={e => this.setState({alimentacion: this.state.alimentacionrespaldo})}>deshacer</Button></InputGroupAddon>
                                }
                                
                            </InputGroup>
                            {!this.state.validaalimentacion && 
                                <Alert color="danger">
                                    El campo <b>Alimentación</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="3">
                            <Label>Habito alcoholico</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio31" name="inline-radios4" value="Consume" checked={this.state.alcohol==='Consume'} onChange={e => this.setState({alcohol: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio31">Consume</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio32" name="inline-radios4" value="No Consume" checked={this.state.alcohol==='No Consume'} onChange={e => this.setState({alcohol: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio32">No Consume</Label>
                            </FormGroup>
                        
                            <br/><br/>
                            
                            {this.state.alcohol == "Consume" &&
                                <Col md="7">
                                    <InputGroup size="lg">
                                        <InputGroupAddon addonType="prepend">
                                            Frecuencia
                                        </InputGroupAddon>
                                        <Input 
                                            size="lg"
                                            type="number"
                                            min="1"
                                            name="acantidad"
                                            value={this.state.acantidad}
                                            onChange={this.cambiaEstadoHabitos}
                                        />
                                        <InputGroupAddon addonType="append">
                                            {this.state.acantidadlabel}
                                        </InputGroupAddon>
                                        <InputGroupAddon addonType="append">
                                            <Dropdown size="lg" isOpen={this.state.dropdownHabitos[0]} toggle={() => {this.toggleHabitos(0)}}>
                                                <DropdownToggle caret>
                                                    {this.state.afrecuencia}
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    <DropdownItem header>Frecuencia</DropdownItem>
                                                    <DropdownItem value="al día" onClick={e => this.setState({afrecuencia: e.target.value})}>al día</DropdownItem>
                                                    <DropdownItem value="a la semana" onClick={e => this.setState({afrecuencia: e.target.value})}>a la semana</DropdownItem>
                                                    <DropdownItem value="al mes" onClick={e => this.setState({afrecuencia: e.target.value})}>al mes</DropdownItem>
                                                    <DropdownItem value="al año" onClick={e => this.setState({afrecuencia: e.target.value})}>al año</DropdownItem>
                                                </DropdownMenu>
                                            </Dropdown>
                                        </InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validaalcohol && 
                                        <Alert color="danger">
                                            Revisa los datos de la <b>Frecuencia de hábito alcohólico</b>
                                        </Alert>
                                    }
                                </Col>
                            }
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="3">
                            <Label>Habito tabáquico</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio41" name="inline-radios5" value="Consume" checked={this.state.tabaco==='Consume'} onChange={e => this.setState({tabaco: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio41">Consume</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio42" name="inline-radios5" value="No Consume" checked={this.state.tabaco==='No Consume'} onChange={e => this.setState({tabaco: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio42">No Consume</Label>
                            </FormGroup>
                        
                            <br/><br/>
                            {this.state.tabaco == "Consume" &&
                                <Col md="7" >
                                    <InputGroup size="lg">
                                        <InputGroupAddon addonType="prepend">
                                            Frecuencia
                                        </InputGroupAddon>
                                        <Input 
                                            size="lg"
                                            type="number"
                                            min="1"
                                            name="tcantidad"
                                            value={this.state.tcantidad}
                                            onChange={this.cambiaEstadoHabitos}
                                        />
                                        <InputGroupAddon addonType="append">
                                            {this.state.tcantidadlabel}
                                        </InputGroupAddon>
                                        <InputGroupAddon addonType="append">
                                        <Dropdown size="lg" isOpen={this.state.dropdownHabitos[1]} toggle={() => {this.toggleHabitos(1)}}>
                                            <DropdownToggle caret>
                                                {this.state.tfrecuencia}
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                <DropdownItem header>Frecuencia</DropdownItem>
                                                <DropdownItem value="al día" onClick={e => this.setState({tfrecuencia: e.target.value})}>al día</DropdownItem>
                                                <DropdownItem value="a la semana" onClick={e => this.setState({tfrecuencia: e.target.value})}>a la semana</DropdownItem>
                                                <DropdownItem value="al mes" onClick={e => this.setState({tfrecuencia: e.target.value})}>al mes</DropdownItem>
                                                <DropdownItem value="al año" onClick={e => this.setState({tfrecuencia: e.target.value})}>al año</DropdownItem>
                                            </DropdownMenu>
                                        </Dropdown>
                                        </InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validatabaco && 
                                        <Alert color="danger">
                                            Revisa los datos de la <b>Frecuencia de hábito tabáquico</b>
                                        </Alert>
                                    }
                                </Col>
                            }
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="4">
                            <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                    Somnia
                                </InputGroupAddon>
                                <Input 
                                    size="lg"
                                    type="number"
                                    min="1"
                                    value={this.state.somnia}
                                    onChange={e => this.setState({somnia: e.target.value, validasomnia: true})}
                                />
                                <InputGroupAddon addonType="append">horas al día</InputGroupAddon>
                            </InputGroup>
                            {!this.state.validasomnia && 
                                <Alert color="danger">
                                    Valor de <b>Somnia</b> no válido
                                </Alert>
                            }
                        </Col>

                        <Col md="4">
                            <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                    Diuresis
                                </InputGroupAddon>
                                <Input 
                                    size="lg"
                                    type="number"
                                    min="1"
                                    value={this.state.diuresis}
                                    onChange={e => this.setState({diuresis: e.target.value, validadiuresis: true})}
                                />
                                <InputGroupAddon addonType="append">veces al día</InputGroupAddon>
                            </InputGroup>
                            {!this.state.validadiuresis && 
                                <Alert color="danger">
                                    Valor de <b>Diuresis</b> no válido
                                </Alert>
                            }
                        </Col>

                        <Col md="4">
                            <InputGroup size="lg">
                                <InputGroupAddon addonType="prepend">
                                    Catarsis
                                </InputGroupAddon>
                                <Input 
                                    size="lg"
                                    type="number"
                                    min="1"
                                    value={this.state.hintestinal}
                                    onChange={e => this.setState({hintestinal: e.target.value, validahintestinal: true})}
                                />
                                <InputGroupAddon addonType="append">veces al día</InputGroupAddon>
                            </InputGroup>
                            {!this.state.validahintestinal && 
                                <Alert color="danger">
                                    Valor de <b>Hábito intestinal</b> no válido
                                </Alert>
                            }
                        </Col>
                        
                    </FormGroup>

                    <div class="prueba-hr">
                        <h4 class="seccion">Antecedentes personales patológicos</h4>
                        <hr class="hr-dinamico"/>
                    </div>

                    <FormGroup row>
                        <Col md="1">
                            <FormGroup check classNam="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox0" name="combe" checked={this.state.combe} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox0"><div class="combet"><b>COMBE</b></div></Label>
                            </FormGroup>
                        </Col>
                        {this.state.combe &&
                            <Col md="6">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        Convive con una persona con: 
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="text"
                                        defaultValue={this.state.combedesc}
                                        onChange={e => this.setState({combedesc: e.target.value, validacombe: true})}
                                    />
                                </InputGroup>
                                {!this.state.validacombe && 
                                    <Alert color="danger">
                                        El campo de <b>COMBE</b> no puede estar vacío
                                    </Alert>
                                }
                            </Col>
                        }
                    </FormGroup>

                    <FormGroup row>
                        <Col md="6">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox1" name="hipertension" checked={this.state.hipertension} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox1">Hipertensión</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox2" name="tbcpulmonar" checked={this.state.tbcpulmonar} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox2">TBC Pulmonar</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox3" name="diabetes" checked={this.state.diabetes} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox3">Diabetes</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox4" name="pmamaria" checked={this.state.pmamaria} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox4">Patología mamaria</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox5" name="cardiopatias" checked={this.state.cardiopatias} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox5">CardioPatías</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox6" name="ets" checked={this.state.ets} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox6">E.T.S.</Label>
                            </FormGroup>                       
                        </Col>

                        <Col md="6">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox7" name="varices" checked={this.state.varices} onChange={this.cambiaEstadoCheckBox} />
                                <Label check className="form-check-label" htmlFor="checkbox7">Varices</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox8" name="erenal" checked={this.state.erenal} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox8">Enf. Renal</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox9" name="hepatopatias" checked={this.state.hepatopatias} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox9">Hepatopatías</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox10" name="tgenitales" checked={this.state.tgenitales} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox10">Tumores Genitales</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox11" name="egastrointestinal" checked={this.state.egastrointestinal} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox11">Enf. Gastrointestinal</Label>
                            </FormGroup>                       
                        </Col>

                        <Col md="1">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox12" name="otrossw" checked={this.state.otrossw} onChange={this.cambiaEstadoCheckBoxOtros}/>
                                <Label check className="form-check-label" htmlFor="checkbox12"><div class="combet"><b>Otros</b></div></Label>
                            </FormGroup>     
                        </Col>
                        {this.state.otrossw &&
                            <Col>
                                {this.state.otros.map((item, idx, vector) => (
                                    <FormGroup row key={idx}>
                                        <Col md="4">
                                            <InputGroup size="lg">
                                                <Input 
                                                    size="lg"
                                                    type="text"
                                                    value={item.enfermedad}
                                                    onChange={this.adicionaOtrosSetState(idx)}
                                                />
                                            
                                                <InputGroupAddon addonType="append">
                                                    <Button color="success" onClick={this.adicionaOtrosc}>+</Button>
                                                </InputGroupAddon>
                                                {idx>0 &&
                                                    <InputGroupAddon addonType="append">
                                                            <Button color="danger" onClick={this.eliminaOtrosc(idx)}>-</Button>
                                                    </InputGroupAddon>
                                                }   
                                            </InputGroup>
                                            {!this.state.validaotros[idx].valida && 
                                                <Alert style={{fontSize: '0.875rem'}} color="danger">
                                                    No puedes dejar este campo de <b>Otros</b> vacío
                                                </Alert>
                                            }
                                        </Col>
                                    </FormGroup>
                                ))}
                                
                            </Col>
                        }
                    </FormGroup>

                    <FormGroup row>
                        <Col md="3">
                            <Label>Antecedentes Quirúrgicos</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radioq1" name="inline-radiosq" value="Si" checked={this.state.aquirurgicossw==='Si'} onChange={e => this.setState({aquirurgicossw: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radioq1">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-label" type="radio" id="inline-radioq2" name="inline-radiosq" value="No" checked={this.state.aquirurgicossw==='No'} onChange={e => this.setState({aquirurgicossw: e.target.value, aquirurgicos: [{tratamientoq: ''}], aquirurgicosano: [{ano: 1900}], validaaquirurgicos: [{validaq: true}]})}/>
                                <Label check className="form-check-label" htmlFor="inline-radioq2">No</Label>
                            </FormGroup>

                            <br/><br/>

                            {this.state.aquirurgicossw == "Si" &&
                                <div>
                                    {this.state.aquirurgicos.map((item, idx) => (
                                    <FormGroup row>
                                    <Col md="12">
                                        <InputGroup size="lg" key={idx}>
                                            <InputGroupAddon addonType="prepend">
                                                T. Quirúrgico
                                            </InputGroupAddon>
                                            <Input 
                                                size="lg"
                                                type="text"
                                                value={item.tratamientoq}
                                                onChange={this.adicionaAQuirurgicosSetState(idx)}
                                            />
                                            <InputGroupAddon addonType="append">
                                                Año
                                            </InputGroupAddon>
                                            <InputGroupAddon addonType="append">
                                                <Input 
                                                    size="lg"
                                                    type="number"
                                                    min="1900"
                                                    value={this.state.aquirurgicosano[idx].ano}
                                                    onChange={this.adicionaAQuirurgicosAnoSetState(idx)}
                                                />
                                            </InputGroupAddon>
                                            <InputGroupAddon addonType="append">
                                                <Button color="success" onClick={this.adicionaAQuirurgicos}>+</Button>
                                            </InputGroupAddon>
                                            {idx>0 &&
                                                <InputGroupAddon addonType="append">
                                                    <Button color="danger" onClick={this.eliminaAQuirurgicos(idx)}>-</Button>
                                                </InputGroupAddon>
                                            }
                                        </InputGroup>
                                    {!this.state.validaaquirurgicos[idx].validaq && 
                                        <Alert color="danger">
                                            Revisa el campo <b>Antecedentes quirúrgicos</b>
                                        </Alert>
                                    }
                                    </Col>
                                    </FormGroup>
                                    ))}
                                </div>
                            }
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="3">
                            <Label>Alergias</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radioa1" name="inline-radiosa" value="Si" checked={this.state.alergiassw==='Si'} onChange={e => this.setState({alergiassw: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radioa1">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radioa2" name="inline-radiosa" value="No" checked={this.state.alergiassw==='No'} onChange={e => this.setState({alergiassw: e.target.value, alergias: [{alergia: ''}], validaalergias: [{validaa: true}]})}/>
                                <Label check className="form-check-label" htmlFor="inline-radioa2">No</Label>
                            </FormGroup>

                            <br/><br/>

                            {this.state.alergiassw == 'Si' &&
                                <div>
                                    {this.state.alergias.map((item, idx) => (
                                    <FormGroup row>
                                    <Col md="12">
                                        <InputGroup size="lg" key={idx}>
                                            <InputGroupAddon addonType="prepend">
                                                Alergia
                                            </InputGroupAddon>
                                            <Input 
                                                size="lg"
                                                type="text"
                                                value={item.alergia}
                                                onChange={this.adicionaAlergiaSetState(idx)}
                                            />
                                            {/* <InputGroupAddon addonType="append">
                                                Descripcion
                                            </InputGroupAddon>
                                            <InputGroupAddon addonType="append">
                                                <Input
                                                    size="lg"
                                                    type="text"
                                                    value={this.state.alergiasd[idx].descripcion}
                                                    onChange={this.adicionaAlergiaDescripcionSetState(idx)}
                                                />
                                            </InputGroupAddon> */}
                                            <InputGroupAddon addonType="append">
                                                <Button color="success" onClick={this.adicionaAlergia}>+</Button>
                                            </InputGroupAddon>
                                            {idx>0 &&
                                                <InputGroupAddon addonType="append">
                                                    <Button color="danger" onClick={this.eliminaAlergia(idx)}>-</Button>
                                                </InputGroupAddon>
                                            }
                                        </InputGroup>
                                    {!this.state.validaalergias[idx].validaa && 
                                        <Alert color="danger">
                                            Este campo de <b>Alergias</b> no puede estar vacío
                                        </Alert>
                                    }
                                    </Col>
                                    </FormGroup>
                                    ))}
                                </div>
                            }
                            
                        </Col>
                    </FormGroup>

                  
                    <FormGroup row>
                        <Col md="3">
                            <Label>Transfusiones</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radiot1" name="inline-radiost" value="Si" checked={this.state.transfusionessw==='Si'} onChange={e => this.setState({transfusionessw: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radiot1">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radiot2" name="inline-radiost" value="No" checked={this.state.transfusionessw==='No'} onChange={e => this.setState({transfusionessw: e.target.value, transfusionesano: [{ano: 1900}], transfusionesdes: [{descripcion: ''}], validatransfusiones: [{validat: true}]})}/>
                                <Label check className="form-check-label" htmlFor="inline-radiot2">No</Label>
                            </FormGroup>
                            
                            <br/><br/>
                            {this.state.transfusionessw == 'Si' &&
                                <div>
                                    {this.state.transfusionesano.map((item, idx) => (
                                    <FormGroup row>
                                    <Col md="12">
                                        <InputGroup size="lg" key={idx}>
                                            <InputGroupAddon addonType="prepend">
                                                Año
                                            </InputGroupAddon>
                                            <Input 
                                                size="lg"
                                                type="number"
                                                min="1900"
                                                value={item.ano}
                                                onChange={this.adicionaTransfusionesSetState(idx)}
                                            />
                                            <InputGroupAddon addonType="append">
                                                Descripcion
                                            </InputGroupAddon>
                                            <InputGroupAddon addonType="append">
                                                <Input 
                                                    size="lg"
                                                    type="text"
                                                    value={this.state.transfusionesdes[idx].descripcion}
                                                    onChange={this.adicionaTransfusionesDescripcionSetState(idx)}
                                                />
                                            </InputGroupAddon>
                                            <InputGroupAddon addonType="append">
                                                <Button color="success" onClick={this.adicionaTransfusiones}>+</Button>
                                            </InputGroupAddon>
                                            {idx>0 &&
                                                <InputGroupAddon addonType="append">
                                                    <Button color="danger" onClick={this.eliminaTransfusiones(idx)}>-</Button>
                                                </InputGroupAddon>
                                            }
                                        </InputGroup>
                                    {!this.state.validatransfusiones[idx].validat && 
                                        <Alert color="danger">
                                            Revisa el campo <b>Transfusiones</b>
                                        </Alert>
                                    }
                                    </Col>
                                    </FormGroup>
                                    ))}
                                </div>
                            }
                        </Col>
                    </FormGroup>

                    <div class="prueba-hr">
                        <h4 class="seccion">Antecedentes familiares</h4>
                        <hr class="hr-dinamico"/>
                    </div>

                    <FormGroup row>
                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox21" name="hipertensionf" checked={this.state.hipertensionf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox21">Hipertensión</Label>
                            </FormGroup> 
                        </Col>
                        <Col md="3">  
                            {this.state.hipertensionf && 
                                <Dropdown isOpen={this.state.dropdownOpen[0]} toggle={() => {
                                    this.toggle(0);
                                }}
                                >
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Hipertension | {this.state.hipertensionffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.hipertensionffam}</Badge> : <span>{this.state.hipertensionffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({hipertensionffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({hipertensionffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({hipertensionffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({hipertensionffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({hipertensionffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({hipertensionffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validahipertensionffam && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                            }
                        </Col>
                        
                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox22" name="tbcpulmonarf" checked={this.state.tbcpulmonarf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox22">TBC Pulmonar</Label>
                            </FormGroup>    
                        </Col>
                        <Col md="3">                   
                            {this.state.tbcpulmonarf && 
                                <Dropdown isOpen={this.state.dropdownOpen[1]} toggle={() => {
                                    this.toggle(1);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        TBC Pulmonar | {this.state.tbcpulmonarffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.tbcpulmonarffam}</Badge> : <span>{this.state.tbcpulmonarffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({tbcpulmonarffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({tbcpulmonarffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({tbcpulmonarffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({tbcpulmonarffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({tbcpulmonarffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({tbcpulmonarffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validatbcpulmonarf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>

                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox23" name="diabetesf" checked={this.state.diabetesf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox23">Diabetes</Label>
                            </FormGroup>
                        </Col> 
                        <Col md="3">                   
                            {this.state.diabetesf && 
                                <Dropdown isOpen={this.state.dropdownOpen[2]} toggle={() => {
                                    this.toggle(2);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Diabetes | {this.state.diabetesffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.diabetesffam}</Badge> : <span>{this.state.diabetesffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({diabetesffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({diabetesffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({diabetesffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({diabetesffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({diabetesffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({diabetesffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validadiabetesf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>

                        <Col md="3">            
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox24" name="pmamariaf" checked={this.state.pmamariaf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox24">Patología mamaria</Label>
                            </FormGroup>                       
                        </Col>
                        <Col md="3">                   
                            {this.state.pmamariaf && 
                                <Dropdown isOpen={this.state.dropdownOpen[3]} toggle={() => {
                                    this.toggle(3);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        P. Mamaria | {this.state.pmamariaffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.pmamariaffam}</Badge> : <span>{this.state.pmamariaffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({pmamariaffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({pmamariaffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({pmamariaffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({pmamariaffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({pmamariaffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({pmamariaffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validapmamariaf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>

                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox25" name="cardiopatiasf" checked={this.state.cardiopatiasf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox25">CardioPatías</Label>
                            </FormGroup>  
                        </Col>    
                        <Col md="3">                   
                            {this.state.cardiopatiasf && 
                                <Dropdown isOpen={this.state.dropdownOpen[4]} toggle={() => {
                                    this.toggle(4);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Cardiopatías | {this.state.cardiopatiasffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.cardiopatiasffam}</Badge> : <span>{this.state.cardiopatiasffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({cardiopatiasffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({cardiopatiasffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({cardiopatiasffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({cardiopatiasffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({cardiopatiasffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({cardiopatiasffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validacardiopatiasf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>

                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox26" name="etsf" checked={this.state.etsf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox26">E.T.S.</Label>
                            </FormGroup>
                        </Col>
                        <Col md="3">                   
                            {this.state.etsf && 
                                <Dropdown isOpen={this.state.dropdownOpen[5]} toggle={() => {
                                    this.toggle(5);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        E.T.S. | {this.state.etsffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.etsffam}</Badge> : <span>{this.state.etsffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({etsffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({etsffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({etsffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({etsffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({etsffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({etsffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validaetsf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>              

                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox27" name="varicesf" checked={this.state.varicesf} onChange={this.cambiaEstadoCheckBox} />
                                <Label check className="form-check-label" htmlFor="checkbox27">Varices</Label>
                            </FormGroup>                       
                        </Col>
                        <Col md="3">                   
                            {this.state.varicesf && 
                                <Dropdown isOpen={this.state.dropdownOpen[6]} toggle={() => {
                                    this.toggle(6);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Varices | {this.state.varicesffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.varicesffam}</Badge> : <span>{this.state.varicesffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({varicesffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({varicesffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({varicesffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({varicesffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({varicesffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({varicesffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validavaricesf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>            
                        
                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox28" name="erenalf" checked={this.state.erenalf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox28">Enf. Renal</Label>
                            </FormGroup>  
                        </Col>
                        <Col md="3">                   
                            {this.state.erenalf && 
                                <Dropdown isOpen={this.state.dropdownOpen[7]} toggle={() => {
                                    this.toggle(7);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Enf. Renal | {this.state.erenalffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.erenalffam}</Badge> : <span>{this.state.erenalffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({erenalffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({erenalffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({erenalffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({erenalffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({erenalffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({erenalffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validaerenalf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col> 

                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox29" name="hepatopatiasf" checked={this.state.hepatopatiasf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox29">Hepatopatías</Label>
                            </FormGroup>                       
                        </Col>
                        <Col md="3">                   
                            {this.state.hepatopatiasf && 
                                <Dropdown isOpen={this.state.dropdownOpen[8]} toggle={() => {
                                    this.toggle(8);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Hepatopatías | {this.state.hepatopatiasffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.hepatopatiasffam}</Badge> : <span>{this.state.hepatopatiasffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({hepatopatiasffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({hepatopatiasffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({hepatopatiasffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({hepatopatiasffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({hepatopatiasffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({hepatopatiasffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validahepatopatiasf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>

                        <Col md="3">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox210" name="tgenitalesf" checked={this.state.tgenitalesf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox210">Tumores Genitales</Label>
                            </FormGroup> 
                        </Col>
                        <Col md="3">                   
                            {this.state.tgenitalesf && 
                                <Dropdown isOpen={this.state.dropdownOpen[9]} toggle={() => {
                                    this.toggle(9);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        T. Genitales | {this.state.tgenitalesffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.tgenitalesffam}</Badge> : <span>{this.state.tgenitalesffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({tgenitalesffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({tgenitalesffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({tgenitalesffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({tgenitalesffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({tgenitalesffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({tgenitalesffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validatgenitalesf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>  
                        <Col md="3">               
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox211" name="egastrointestinalf" checked={this.state.egastrointestinalf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox211">Enf. Gastrointestinal</Label>
                            </FormGroup>                       
                        </Col>
                        <Col md="3">                   
                            {this.state.egastrointestinalf && 
                                <Dropdown isOpen={this.state.dropdownOpen[10]} toggle={() => {
                                    this.toggle(10);
                                }}>
                                    <DropdownToggle style={{width: '100%'}} caret>
                                        Enf. Gastro-Int. | {this.state.egastrointestinalffam !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.egastrointestinalffam}</Badge> : <span>{this.state.egastrointestinalffam}</span>}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem header>Familiar</DropdownItem>
                                        <DropdownItem value="Hermano" onClick={e => this.setState({egastrointestinalffam: e.target.value})}>Hermano</DropdownItem>
                                        <DropdownItem value="Hermana" onClick={e => this.setState({egastrointestinalffam: e.target.value})}>Hermana</DropdownItem>
                                        <DropdownItem value="Padre" onClick={e => this.setState({egastrointestinalffam: e.target.value})}>Padre</DropdownItem>
                                        <DropdownItem value="Madre" onClick={e => this.setState({egastrointestinalffam: e.target.value})}>Madre</DropdownItem>
                                        <DropdownItem value="Abuelos" onClick={e => this.setState({egastrointestinalffam: e.target.value})}>Abuelos</DropdownItem>
                                        <DropdownItem value="Otro Familiar" onClick={e => this.setState({egastrointestinalffam: e.target.value})}>Otro familiar</DropdownItem>
                                    </DropdownMenu>
                                {!this.state.validaegastrointestinalf && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Selecciona un <b>Familiar</b>
                                    </Alert>
                                }
                                </Dropdown>
                                }
                        </Col>

                        <Col md="6"></Col>
                        <Col md="1">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox212" name="otrosfamiliarsw" checked={this.state.otrosfamiliarsw} onChange={this.cambiaEstadoCheckBoxOtrosFamiliar}/>
                                <Label check className="form-check-label" htmlFor="checkbox212"><div class="combet"><b>Otros</b></div></Label>
                            </FormGroup>     
                        </Col>
                        <Col md="9">
                            {this.state.otrosfamiliarsw && 
                                <Col md="12">
                                    {this.state.otrosfamiliar.map((item, idx) => (
                                        <FormGroup row key={idx}>
                                            <Col md="12">
                                                <InputGroup size="lg">
                                                    <InputGroupAddon addonType="prepend">
                                                        Enfermedad
                                                    </InputGroupAddon>
                                                    <Input
                                                        size="lg"
                                                        type="text"
                                                        value={this.state.otrosfamiliarenf[idx].enfermedad}
                                                        onChange={this.adicionaOtrosFamiliarEnfermedadSetState(idx)}
                                                    />
                                                    <InputGroupAddon addonType="append">
                                                        <Dropdown size="lg" isOpen={this.state.otrosfamiliardropdown[idx].dropdownotrosf} toggle={() => {this.toggleOtrosFamiliar(idx)}}>
                                                            <DropdownToggle caret>
                                                                {this.state.otrosfamiliar[idx].familiar !== 'Sin asignar' ? <Badge style={{fontSize: '0.875rem'}} color="primary">{this.state.otrosfamiliar[idx].familiar}</Badge> : <span>{this.state.otrosfamiliar[idx].familiar}</span>}
                                                            </DropdownToggle>
                                                            <DropdownMenu>
                                                                <DropdownItem header>Familiar</DropdownItem>
                                                                <DropdownItem value="Hermano" onClick={this.adicionaOtrosFamiliarSetState(idx)}>Hermano</DropdownItem>
                                                                <DropdownItem value="Hermana" onClick={this.adicionaOtrosFamiliarSetState(idx)}>Hermana</DropdownItem>
                                                                <DropdownItem value="Padre" onClick={this.adicionaOtrosFamiliarSetState(idx)}>Padre</DropdownItem>
                                                                <DropdownItem value="Madre" onClick={this.adicionaOtrosFamiliarSetState(idx)}>Madre</DropdownItem>
                                                                <DropdownItem value="Abuelos" onClick={this.adicionaOtrosFamiliarSetState(idx)}>Abuelos</DropdownItem>
                                                                <DropdownItem value="Otro familiar" onClick={this.adicionaOtrosFamiliarSetState(idx)}>Otro familiar</DropdownItem>
                                                            </DropdownMenu>
                                                        </Dropdown>
                                                    </InputGroupAddon>
                                                    <InputGroupAddon addonType="append">
                                                        <Button color="success" onClick={this.adicionaOtrosFamiliar}>+</Button>
                                                    </InputGroupAddon>
                                                    {idx>0 &&
                                                        <InputGroupAddon addonType="append">
                                                            <Button color="danger" onClick={this.eliminaOtrosFamiliar(idx)}>-</Button>
                                                        </InputGroupAddon>
                                                    }
                                                </InputGroup>
                                                {!this.state.validaotrosfamiliar[idx].validaf && 
                                                    <Alert color="danger">
                                                        Revisa este campo de <b>Antecedentes familiares</b>
                                                    </Alert>
                                                }
                                            </Col>
                                        </FormGroup>
                                    ))}
                                </Col>
                            }
                        </Col>
                    </FormGroup>
                    {this.props.paciente.id_sexo=='F' ?
                        <div class="prueba-hr">
                            <h4 class="seccion">Antecedentes gineco obstétricos</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        :
                        <div class="prueba-hr" style={{opacity:'0.4'}}>
                            <h4 class="seccion">Antecedentes gineco obstétricos</h4>
                            <hr class="hr-dinamico"/>
                            <p>Deshabilitado</p>
                        </div>
                    }
                    {this.props.paciente.id_sexo=='F' &&
                        <FormGroup row>
                            <Col md="3">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        Gesta
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="number"
                                        min="1"
                                        onChange={e => this.setState({gesta: e.target.value})}
                                    />
                                    <InputGroupAddon addonType="append">
                                        .
                                    </InputGroupAddon>
                                </InputGroup>
                            </Col>
                            <Col md="3">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        Menarca
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="number"
                                        min="1"
                                        onChange={e => this.setState({menarca: e.target.value, validamenarca: true})}
                                    />
                                    <InputGroupAddon addonType="append">
                                        años.
                                    </InputGroupAddon>
                                </InputGroup>
                                {!this.state.validamenarca && 
                                    <Alert style={{fontSize: '0.875rem'}} color="danger">
                                        Proporciona un dato válido
                                    </Alert>
                                }
                            </Col>
                            <Col md="3">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        Para
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="text"
                                        onChange={e => this.setState({para: e.target.value})}
                                    />
                                </InputGroup>
                            </Col>
                            <Col md="3">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        F.U.M.
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="date"
                                        onChange={e => this.setState({fum: e.target.value})}
                                    />
                                </InputGroup>
                            </Col>
                            <Col md="3">
                                        <InputGroup size="lg">
                                            <InputGroupAddon addonType="prepend">
                                                Abortos
                                            </InputGroupAddon>
                                            <Input
                                                size="lg"
                                                type="number"
                                                min="1"
                                                onChange={e => this.setState({abortos: e.target.value})}
                                            />
                                            <InputGroupAddon addonType="append">.</InputGroupAddon>
                                        </InputGroup>
                            </Col>
                            <Col md="3">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        Catamenio
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="number"
                                        min="1"
                                        onChange={e => this.setState({catamenio: e.target.value})}
                                    />
                                    <InputGroupAddon addonType="append">.</InputGroupAddon>
                                </InputGroup>
                            </Col>
                            <Col md="3">
                                <InputGroup size="lg">
                                    <InputGroupAddon addonType="prepend">
                                        I.R.S.
                                    </InputGroupAddon>
                                    <Input
                                        size="lg"
                                        type="number"
                                        min="1"
                                        onChange={e => this.setState({irs: e.target.value})}
                                    />
                                    <InputGroupAddon addonType="append">
                                        años.
                                    </InputGroupAddon>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                    }

                    <div class="prueba-hr">
                        <h4 class="seccion">Planificación familiar</h4>
                        <hr class="hr-dinamico"/>
                    </div>

                    <FormGroup row>
                        <Col md="3">
                            <Label>¿Hubo planificación familiar?</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radiopf1" name="inline-radiospf" value="Si" checked={this.state.planificacionf==='Si'} onChange={e => this.setState({planificacionf: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radiopf1">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radiopf2" name="inline-radiospf" value="No" checked={this.state.planificacionf==='No'} onChange={e => this.setState({planificacionf: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radiopf2">No</Label>
                            </FormGroup>

                            <br/><br/>

                             {this.state.planificacionf == 'Si' &&
                             <FormGroup row>
                                <Col md="12">
                                        <InputGroup size="lg">
                                            <InputGroupAddon addonType="prepend">
                                                Método
                                            </InputGroupAddon>
                                                <Input
                                                    size="lg"
                                                    type="text"
                                                    onChange={e => this.setState({metodo: e.target.value, validametodo: true})}
                                                />
                                        </InputGroup>
                                    {!this.state.validametodo && 
                                        <Alert color="danger">
                                            El campo <b>Método</b> de planificación familiar no debe estar vacío
                                        </Alert>
                                    }
                                </Col>
                                <Col md="12">
                                        <InputGroup size="lg">
                                            <InputGroupAddon addonType="prepend">
                                                Continua
                                            </InputGroupAddon>
                                            <Input
                                                size="lg"
                                                type="text"
                                                onChange={e => this.setState({continua: e.target.value})}
                                            />
                                        </InputGroup>
                                </Col>
                                <Col md="12">
                                        <InputGroup size="lg">
                                            <InputGroupAddon addonType="prepend">
                                                Causa de abandono
                                            </InputGroupAddon>
                                            <Input
                                                size="lg"
                                                type="text"
                                                onChange={e => this.setState({cabandono: e.target.value})}
                                            />
                                        </InputGroup>
                                </Col>
                            </FormGroup>
                             }
                        </Col>
                    </FormGroup>

                    <div class="prueba-hr">
                        <h4 class="seccion">Recolección de datos</h4>
                        <hr class="hr-dinamico"/>
                    </div>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Motivo Consulta</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input class="input"
                                size="lg"
                                type="text"
                                placeholder="Ej. Dolor de cabeza, Dolor abdominal, etc..."
                                onChange={e => this.setState({consulta: e.target.value, validaconsulta: true})}
                            />
                            {!this.state.validaconsulta && 
                                <Alert color="danger">
                                    El campo <b>Motivo de Consulta</b> es obligatorio
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Historia de la Enfermedad Actual</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                size="lg"
                                type="textarea"
                                placeholder="Ej. Dolor presente desde hace 3 dias..."
                                rows="3"
                                onChange={e => this.setState({historia: e.target.value, validahistoria: true})}
                            />
                            {!this.state.validahistoria && 
                                <Alert color="danger">
                                    Debes indicar la <b>Historia de la Enfermedad Actual</b>
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    
                    <div class="prueba-hr">
                        <h4 class="seccion">Signos vitales</h4>
                        <hr class="hr-dinamico"/>
                    </div>

                    <FormGroup row>
                    <Col md="12">
                        <Table responsive bordered>
                            <thead style={{textAlign: 'center', backgroundColor: '#005073', color:'#fff'}}>
                                <th>Presión arterial</th>
                                <th>Peso</th>
                                <th>Pulso</th>
                                <th>Talla</th>
                                <th>Temperatura</th>
                            </thead>
                            <tbody>
                                <td>
                                    <InputGroup size="lg">
                                    <Input 
                                        size="lg"
                                        type="number"
                                        value={this.state.pa}
                                        min="1"
                                        onChange={e => this.setState({pa: e.target.value, validapa: true})}
                                    />
                                    <InputGroupAddon addonType="append">
                                        .
                                    </InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validapa && 
                                        <Alert style={{fontSize: '0.875rem'}} color="danger">
                                            Valor de <b>P.A.</b> no válido
                                        </Alert>
                                    }
                                </td>
                                <td>
                                    <InputGroup size="lg">
                                    <Input 
                                        size="lg"
                                        type="number"
                                        min="1"
                                        value={this.state.peso}
                                        onChange={e => this.setState({peso: e.target.value, validapeso: true})}
                                    />
                                    <InputGroupAddon addonType="append">kg.</InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validapeso && 
                                        <Alert style={{fontSize: '0.875rem'}} color="danger">
                                            Valor de <b>Peso</b> no válido
                                        </Alert>
                                    }
                                </td>
                                <td>
                                    <InputGroup size="lg">
                                    <Input 
                                        size="lg"
                                        type="number"
                                        min="1"
                                        value={this.state.pulso}
                                        onChange={e => this.setState({pulso: e.target.value, validapulso: true})}
                                    />
                                    <InputGroupAddon addonType="append">lpm.</InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validapulso && 
                                        <Alert style={{fontSize: '0.875rem'}} color="danger">
                                            Valor de <b>Pulso</b> no válido
                                        </Alert>
                                    }
                                </td>
                                <td>
                                    <InputGroup size="lg">
                                    <Input 
                                        size="lg"
                                        type="number"
                                        min="1"
                                        value={this.state.talla}
                                        onChange={e => this.setState({talla: e.target.value, validatalla: true})}
                                    />
                                    <InputGroupAddon addonType="append">cms.</InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validatalla && 
                                        <Alert style={{fontSize: '0.875rem'}} color="danger">
                                            Valor de <b>Talla</b> no válido
                                        </Alert>
                                    }
                                </td>
                                <td>
                                    <InputGroup size="lg">
                                    <Input 
                                        size="lg"
                                        type="number"
                                        min="1"
                                        value={this.state.temperatura}
                                        onChange={e => this.setState({temperatura: e.target.value, validatemperatura: true})}
                                    />
                                    <InputGroupAddon addonType="append">ºC</InputGroupAddon>
                                    </InputGroup>
                                    {!this.state.validatemperatura && 
                                        <Alert style={{fontSize: '0.875rem'}} color="danger">
                                            Valor de <b>Temperatura</b> no válido
                                        </Alert>
                                    }
                                </td>
                            </tbody>
                        </Table>
                    </Col>
                    </FormGroup>
                    
                    <div class="prueba-hr">
                        <h4 class="seccion">Examen físico</h4>
                        <hr class="hr-dinamico"/>
                    </div>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Estado general y grado de conciencia</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.gconciencia}
                                onChange={e => this.setState({gconciencia: e.target.value, validagconciencia: true})}
                                rows="4"
                            />
                            {!this.state.validagconciencia && 
                                <Alert color="danger">
                                    El campo <b>Estado general y grado de conciencia</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Piel y faneras</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.pielfaneras}
                                onChange={e => this.setState({pielfaneras: e.target.value, validapielfaneras: true})}
                                rows="4"
                            />
                            {!this.state.validapielfaneras && 
                                <Alert color="danger">
                                    El campo <b>Piel y faneras</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Cara y cuello</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.caracuello}
                                onChange={e => this.setState({caracuello: e.target.value, validacaracuello: true})}
                                rows="5"
                            />
                            {!this.state.validacaracuello && 
                                <Alert color="danger">
                                    El campo <b>Cara y cuello</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Visual y auditivo</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.visualauditivo}
                                onChange={e => this.setState({visualauditivo: e.target.value, validavisualauditivo: true})}
                                rows="4"
                            />
                            {!this.state.validavisualauditivo && 
                                <Alert color="danger">
                                    El campo <b>Visual y auditivo</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Tiroides</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.tiroides}
                                onChange={e => this.setState({tiroides: e.target.value, validatiroides: true})}
                                rows="4"
                            />
                            {!this.state.validatiroides && 
                                <Alert color="danger">
                                    El campo <b>Tiroides</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Cardiopulmonar</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.cardiopulmonar}
                                onChange={e => this.setState({cardiopulmonar: e.target.value, validacardiopulmonar: true})}
                                rows="4"
                            />
                            {!this.state.validacardiopulmonar && 
                                <Alert color="danger">
                                    El campo <b>Cardiopulmonar</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Abdomen</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.abdomen}
                                onChange={e => this.setState({abdomen: e.target.value, validaabdomen: true})}
                                rows="4"
                            />
                            {!this.state.validaabdomen && 
                                <Alert color="danger">
                                    El campo <b>Abdomen</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Genitales</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.genitales}
                                onChange={e => this.setState({genitales: e.target.value, validagenitales: true})}
                                rows="4"
                            />
                            {!this.state.validagenitales && 
                                <Alert color="danger">
                                    El campo <b>Genitales</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Extremidades</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.extremidades}
                                onChange={e => this.setState({extremidades: e.target.value, validaextremidades: true})}
                                rows="4"
                            />
                            {!this.state.validaextremidades && 
                                <Alert color="danger">
                                    El campo <b>Extremidades</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Neurológico</Label>
                        </Col>
                        <Col md="9">
                            <Input
                                size="lg"
                                type="textarea"
                                value={this.state.neurologico}
                                onChange={e => this.setState({neurologico: e.target.value, validaneurologico: true})}
                                rows="4"
                            />
                            {!this.state.validaneurologico && 
                                <Alert color="danger">
                                    El campo <b>Neurológico</b> no puede estar vacío
                                </Alert>
                            }
                        </Col>
                    </FormGroup>



                    <FormGroup row>
                        <Col md="3">
                            <h5>Datos Complementarios</h5>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                size="lg"
                                type="textarea"
                                placeholder="El paciente tiene dificultades para hablar..."
                                onChange={e => this.setState({datosc: e.target.value})}
                            />
                        </Col>
                    </FormGroup>

                    <hr class="diagnostico"/>

                    <div class="diagnosticop">
                        <FormGroup row>
                            <Col md="3">
                                <h5 style={{marginBottom: '0'}}>Diagnóstico Presuntivo</h5>
                                <h5>[CIE - 10]</h5>
                            </Col>
                            <Col md="9">
                                <Search 
                                    data={ this.state.cie10 }
                                    onChange={ this.cie10.bind(this) }
                                    placeholder="Diagnostico CIE10..."
                                    class="search-class"
                                    searchKey="DESCRIPCION CODIGOS DE CUATRO CARACTERES"
                                />
                                {!this.state.validadiagnostico && 
                                    <Alert color="danger">
                                        El paciente debe tener un <b>DIAGNOSTICO</b> correspondiente al <b>CIE - 10</b>.
                                    </Alert>
                                }
                            </Col>
                        </FormGroup>
                        
                    </div>
                    {/* MODAL! */}
                    <Modal isOpen={this.state.modal} toggle={this.toggleModal(this.props.paciente)}
                    className={'modal-success ' + this.props.className} style={{transition: '0.3s'}}>
                        <ModalHeader toggle={this.toggleModal}>
                            Atención
                        </ModalHeader>
                        <ModalBody>
                        ¿Deseas guardar la Historia Clinica del paciente <span style={{textTransform: 'capitalize'}}><b>{(this.props.paciente.primer_apellido + ' ' + this.props.paciente.segundo_apellido + ' ' + this.props.paciente.nombres).toLocaleLowerCase()}</b></span>?
                        </ModalBody>
                        <ModalFooter style={{height: '70px', justifyContent: 'space-between'}}>
                                {this.state.swloader ?
                                <div classname="animated fadeIn">
                                    <BounceLoader 
                                        className={override}
                                        sizeUnit={"px"}
                                        size={40}
                                        color={'#4dbd74'}
                                        loading={this.state.loading}
                                    />
                                    <span style={{color: '#4dbd74'}}><b>Cargando</b></span>
                                </div>
                                :
                                <div></div>
                            } 
                        <div>
                            {/* <Button color="success" onClick={this.enviarInformacionPromise}>Aceptar</Button>{' '} */}
                            <Button color="success" onClick={this.validaHistorial}>Aceptar</Button>{' '}
                            <Button color="secondary" onClick={this.toggleModal(this.state.afiliadox)}>Cancel</Button>
                        </div>
                        </ModalFooter>
                    </Modal>

                    {/* TOASTER!!! */}
                    <ToastContainer 
                        position="bottom-right"
                        autoClose={3000}
                        closeOnClick
                        pauseOnHover
                        newestOnTop
                    />

                    {/* Modal de ERROR :( */}
                    <Modal isOpen={this.state.modalerror} toggle={this.toggleModalError}
                        className={'modal-danger ' + this.props.className}>
                        <ModalBody>
                        <div class="modal-error">
                            <h2>Ups!</h2>
                            <div class="contenedor-img-error"><img src="assets/img/icono-error.png" class="responsivog" style={{maxWidth: '350px'}}></img></div>
                            <p><b>[</b>El documento tiene <b>errores</b><b>]</b></p>
                            <div class="contenedor-btn-error">
                                <NavLink><span onClick={this.toggleModalError}>Entendido</span></NavLink>
                            </div>
                        </div>
                        </ModalBody>
                    </Modal>

                    </CardBody>
                    <CardFooter>
                        <Button block onClick={this.toggleModal(this.props.paciente)} size="lg" color="success">Enviar Historial Clinico</Button>
                    </CardFooter>
                </Card>
                </Col>
            <Col md="12">
            </Col>
            </Row>
            
          </div>
        );
    }
}

const mapStateToProps = state => ({
    paciente: state.paciente,
    afiliadotelefono: state.afiliadotelefono
});

export default connect(mapStateToProps)(HistoriaClinica);