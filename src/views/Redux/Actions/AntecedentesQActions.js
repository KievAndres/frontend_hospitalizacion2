export const UPDATE_ANTECEDENTESQ = 'antecedentesqs:updateAntecedentesQ';

export function updateAntecedentesQ(newAntecedentesQ){
    return{
        type: UPDATE_ANTECEDENTESQ,
        payload: {
            antecedentesq: newAntecedentesQ
        }
    }
}