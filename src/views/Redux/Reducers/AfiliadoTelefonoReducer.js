import { UPDATE_AFILIADOTELEFONO } from '../Actions/AfiliadoTelefonoActions';

export default function afiliadotelefonoReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_AFILIADOTELEFONO:
            return payload.afiliadotelefono;
        default:
            return state;
    }
}