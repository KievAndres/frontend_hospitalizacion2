export const UPDATE_AFILIADOTELEFONO = 'afiliadostel:updateAfiliadoTelefono';

export function updateAfiliadoTelefono(newAfiliadoTelefono){
    return{
        type: UPDATE_AFILIADOTELEFONO,
        payload: {
            afiliadotelefono: newAfiliadoTelefono
        }
    }
}