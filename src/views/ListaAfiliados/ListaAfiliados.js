import React, { Component } from 'react';
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';

import { connect } from 'react-redux';
import { updateUser } from '../Redux/Actions/UserActions';
import { updatePaciente } from '../Redux/Actions/PacienteActions';
import { updateHC } from '../Redux/Actions/TieneHCActions';
import { updateHistorialClinico } from '../Redux/Actions/HistorialClinicoActions';
import { updateAnamnesis } from '../Redux/Actions/AnamnesisActions';
import { updateExamenFisico } from '../Redux/Actions/ExamenFisicoActions';
import { updateLaboratorios } from '../Redux/Actions/LaboratoriosActions';
import { updateAntecedentesPNP } from '../Redux/Actions/AntecedentesPNPActions';
import { updateAntecedentesPP } from '../Redux/Actions/AntecedentesPPActions';
import { updateAntecedentesFam } from '../Redux/Actions/AntecedentesFamiliaresActions';
import { updatePlanificacionF } from '../Redux/Actions/PlanificacionFActions';
import { updateAntecedentesGO } from '../Redux/Actions/AntecedentesGOActions';
import { updateTransfusiones } from '../Redux/Actions/TransfusionesActions';
import { updateAlergias } from '../Redux/Actions/AlergiasActions';
import { updateAntecedentesQ } from '../Redux/Actions/AntecedentesQActions';
import { updateAntecedentesC } from '../Redux/Actions/AntecedentesCActions';
import { updateSignosVitales } from '../Redux/Actions/SignosVitalesActions';

import { css } from 'react-emotion';
import { ClipLoader, BounceLoader } from 'react-spinners';
import { Z_BLOCK } from 'zlib';

// TOASTER
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { updateAfiliadoTelefono } from '../Redux/Actions/AfiliadoTelefonoActions';

const override = css`
  display: Z_BLOCK;
  margin: 0 auto;
  border-color: red;
  `;

class ListaAfiliados extends Component {
    constructor(props){
        super(props)
        this.state = {
            afiliados: [],

            afiliadox: [],
            // -----------
            afiliadotel: [],
            // -----------
            historiac: [],
            // -----------
            anamnesis: [],
            examenf: [],
            laboratorios: [],
            // ----------
            antecedentespnp: [],
            antecedentespp: [],
            antecedentesfam: [],
            planificacionf: [],
            antecedentesgo: [],
            // -----------
            transfusiones: [],
            alergias: [],
            antecedentesq: [],
            antecedentesc: [],

            // -----------
            signosv: [],

            existe_historiac: '',
            search: '',

            modal: false,
            collapse: false,
            collapseafiliados: [],
            swloader: false,
        }

        this.onUpdatePaciente = this.onUpdatePaciente.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    async componentDidMount() {
      try{
          const respuesta = await fetch('http://localhost:8000/api/v1/afiliado/');
          const afiliados = await respuesta.json();
          
          let arrayafiliados=new Array(afiliados.length);
          arrayafiliados.fill(false);
          
          console.log(afiliados);
          console.log(afiliados.length);
          console.log(arrayafiliados);
          this.setState({
            afiliados,
            collapseafiliados: arrayafiliados,
          });
      } catch(e){
          console.log(e);
      }
    }

    updateSearch(event){
      this.setState({
          search: event.target.value.substr(0,30),
          collapseafiliados: this.state.collapseafiliados.fill(false)
        });
    }

    toggleModal = (item) => () => {
      this.setState({
        afiliadox: item,
        modal: !this.state.modal
      })
    }

    toggleCollapse = (id) => () => {
        this.setState({
            collapse: !this.state.collapse
        });
    }

    toggleAccordion(tab) {

        const prevState = this.state.collapseafiliados;
        const state = prevState.map((x, index) => tab === index ? !x : false);
    
        this.setState({
          collapseafiliados: state,
        });
      }


    // onUpdatePaciente = (item) => async() => {
    // }
    
    // Esto se ejecuta cuando aceptas el modal, carga los datos del paciente X
    async onUpdatePaciente(){
        try{
            this.setState({
                swloader: true
            })
            console.log('****************');
            let nombre=this.state.afiliadox.primer_apellido+' '+this.state.afiliadox.segundo_apellido+' '+this.state.afiliadox.nombres;
            console.log(this.state.afiliadox.dip, nombre);
            this.props.onUpdateUser(this.state.afiliadox.dip);
            this.props.onUpdatePaciente(this.state.afiliadox);

            const respuestaat = await fetch('http://localhost:8000/api/v1/afiliadotf/'+this.state.afiliadox.dip+'/');
            const afiliadotel = await respuestaat.json();
            this.props.onUpdateAfiliadoTelefono(afiliadotel);

            let existe_historiac = '';
            let historiaclinica = [];
            if(this.props.dip != 'Indefinido'){
                const respuestavhc = await fetch('http://localhost:8000/api/v1/historiaclinicav/'+this.state.afiliadox.dip+'/');
                const verificahc = await respuestavhc.json();
                console.log(verificahc);
                this.props.onUpdateHC(verificahc.existe);
                //Verifica si existe el HC en la BD
                if(verificahc.existe){
                    const respuestahc = await fetch('http://localhost:8000/api/v1/historiaclinicaf/'+this.state.afiliadox.dip+'/')
                    const historiac = await respuestahc.json();
                    console.log(historiac);
                    this.props.onUpdateHistorialClinico(historiac);
                    // ------------------------------------------------------------------------------
                    const respuestaan = await fetch('http://localhost:8000/api/v1/anamnesisf/'+historiac.id_historiaclinica+'/');
                    const anamnesis = await respuestaan.json();
                    console.log(anamnesis);
                    this.props.onUpdateAnamnesis(anamnesis);
                    const respuestaef = await fetch('http://localhost:8000/api/v1/examenfisicof/'+historiac.id_historiaclinica+'/');
                    const examenf = await respuestaef.json();
                    console.log(examenf);
                    this.props.onUpdateExamenFisico(examenf);
                    const respuestalab = await fetch('http://localhost:8000/api/v1/laboratorios/');
                    const laboratorios = await respuestalab.json();
                    // PENDIENTE
                    // ------------------------------------------------------------------------------
                    const respuestaapnp = await fetch('http://localhost:8000/api/v1/antecedentespnpf/'+anamnesis.id_anamnesis+'/');
                    const antecedentespnp = await respuestaapnp.json();
                    console.log(antecedentespnp);
                    this.props.onUpdateAntecedentesPNP(antecedentespnp);
                    const respuestaapp = await fetch('http://localhost:8000/api/v1/antecedentesppf/'+anamnesis.id_anamnesis+'/');
                    const antecedentespp = await respuestaapp.json();
                    console.log(antecedentespp);
                    this.props.onUpdateAntecedentesPP(antecedentespp);
                    const respuestaafam = await fetch('http://localhost:8000/api/v1/antecedentesffk/'+anamnesis.id_anamnesis+'/');
                    const antecedentesfam = await respuestaafam.json();
                    console.log(antecedentesfam);
                    this.props.onUpdateAntecedentesFam(antecedentesfam);
                    const respuestapf = await fetch('http://localhost:8000/api/v1/planificacionf/');
                    const planificacionf = await respuestapf.json();
                    // Pendiente
                    const antecedentesgo = () => async() => {
                        if(this.props.paciente.id_sexo === 'F'){
                            const respuestaago = await fetch('http://localhost:8000/api/v1/antecedentesgof/'+anamnesis.id_anamnesis+'/');
                            const antecedentesgo = await respuestaago.json();
                            console.log(antecedentesgo);
                            this.props.onUpdateAntecedentesGO(antecedentesgo);
                            return antecedentesgo;
                        }else{
                            return {}
                        }
                    }
                    if(this.props.paciente.id_sexo === 'F'){

                        const respuestaago = await fetch('http://localhost:8000/api/v1/antecedentesgof/'+anamnesis.id_anamnesis+'/');
                        const antecedentesgo = await respuestaago.json();
                        console.log(antecedentesgo);
                        this.props.onUpdateAntecedentesGO(antecedentesgo);
                    }
                    
                    // ------------------------------------------------------------------------------
                    const respuestatr = await fetch('http://localhost:8000/api/v1/transfusionesf/'+antecedentespp.id_app+'/');
                    const transfusiones = await respuestatr.json();
                    console.log(transfusiones);
                    this.props.onUpdateTransfusiones(transfusiones)
                    const respuestaal = await fetch('http://localhost:8000/api/v1/alergiasf/'+antecedentespp.id_app+'/');
                    const alergias = await respuestaal.json();
                    console.log(alergias)
                    this.props.onUpdateAlergias(alergias);
                    const respuestaaq = await fetch('http://localhost:8000/api/v1/antecedentesqf/'+antecedentespp.id_app+'/');
                    const antecedentesq = await respuestaaq.json();
                    console.log(antecedentesq);
                    this.props.onUpdateAntecedentesQ(antecedentesq);
                    const respuestaac = await fetch('http://localhost:8000/api/v1/antecedentescf/'+antecedentespp.id_app+'/');
                    const antecedentesc = await respuestaac.json();
                    console.log(antecedentesc);
                    this.props.onUpdateAntecedentesC(antecedentesc);
                    // ------------------------------------------------------------------------------
                    const respuestasv = await fetch('http://localhost:8000/api/v1/signosvitalesef/'+examenf.id_examenfisico+'/');
                    const signosv = await respuestasv.json();
                    console.log(signosv);
                    this.props.onUpdateSignosVitales(signosv);
                    
                    //Guardando los datos del afiliado
                    if(this.props.paciente.id_sexo === 'F'){
                        this.setState({
                            historiac,
                            anamnesis,
                            examenf,
                            laboratorios,
                            antecedentespnp,
                            antecedentespp,
                            antecedentesfam,
                            planificacionf,
                            antecedentesgo,
                            transfusiones,
                            alergias,
                            antecedentesq,
                            antecedentesc,
                            signosv,
                            modal: false,
                            swloader: false,
                        });
                    }else{
                        this.setState({
                            historiac,
                            anamnesis,
                            examenf,
                            laboratorios,
                            antecedentespnp,
                            antecedentespp,
                            antecedentesfam,
                            planificacionf,
                            transfusiones,
                            alergias,
                            antecedentesq,
                            antecedentesc,
                            signosv,
                            modal: false,
                            swloader: false,
                        });
                    }
                }else{
                    this.setState({
                        modal: false,
                        swloader: false,
                    })
                }
                let p_apellido=this.props.paciente.primer_apellido;
                let s_apellido=this.props.paciente.segundo_apellido;
                toast.error("Datos de "+p_apellido+" "+s_apellido+" cargados");
            }
            // if(this.props.dip != 'Indefinido'){
            //     fetch('http://localhost:8000/api/v1/historiaclinicav/'+this.state.afiliadoAUX.dip+'/')
            //     .then((Response) => Response.json())
            //     .then((findresponse) => {
            //         existe_historiac = findresponse.existe;
            //         this.props.onUpdateHC(existe_historiac);
            //     })
            //     .then(() => {
            //         if(existe_historiac){
            //             fetch('http://localhost:8000/api/v1/historiaclinicaf/'+this.state.afiliadoAUX.dip+'/')
            //             .then((Response) => Response.json())
            //             .then((findresponse) => {
            //                 historiaclinica = findresponse;
            //                 this.props.onUpdateHistorialClinico(historiaclinica);
            //             })
            //             .then(() => {

            //             })
            //             .catch(error => console.log('Error: ',error));
            //         }else{
            //             this.props.onUpdateHistorialClinico('Sin historial');
            //         }
            //         console.log(historiaclinica);
            //     })
            //     .catch(error => console.log('Error: ',error));

                
            // }
            
        }catch(e){
            console.log(e);
        }
    }

    

   render() {
        let afiliados_filtrados = this.state.afiliados.filter(
          (item) =>{
            return (item.primer_apellido + ' ' + item.segundo_apellido + ' ' 
            + item.nombres + ' ' + item.dip + ' ' + item.matricula)
            .toUpperCase().indexOf(
              this.state.search.toUpperCase()) !== -1;
          }
        );
        console.log(this.state.swloader);
        return (
          <div className="animated fadeIn" class="listapacientes">
            <Modal isOpen={this.state.modal} toggle={this.toggleModal(this.state.afiliadox)}
                    className={'modal-danger ' + this.props.className} style={{transition: '0.3s'}}>
                <ModalHeader toggle={this.toggleModal}>Atención</ModalHeader>
                <ModalBody>
                ¿Deseas cargar los datos del paciente <span style={{textTransform: 'capitalize'}}><b>{(this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres).toLocaleLowerCase()}</b></span>?
                
                

                </ModalBody>
                <ModalFooter style={{height: '70px', justifyContent: 'space-between'}}>
                    {this.state.swloader ?
                        <div classname="animated fadeIn">
                            <BounceLoader 
                                className={override}
                                sizeUnit={"px"}
                                size={40}
                                color={'#f86c6b'}
                                loading={this.state.loading}
                            />
                            <span style={{color: '#f86c6b'}}><b>Cargando</b></span>
                        </div>
                        :
                        <div></div>
                    } 
                <div>
                    <Button color="danger" onClick={this.onUpdatePaciente}>Aceptar</Button>{' '}
                    <Button color="secondary" onClick={this.toggleModal(this.state.afiliadox)}>Cancel</Button>
                </div>
                </ModalFooter>
            </Modal>

            {/* TOASTER!!! */}
            <ToastContainer 
                position="bottom-right"
                autoClose={3000}
                closeOnClick
                pauseOnHover
                newestOnTop
            />

            <Row>
            <Col xs="12" sm="12" md="12">
            <Card>
                <CardBody>
                <div class="titulo">
                    <h1>PACIENTES EN COLA DE ESPERA</h1>
                    <hr/>
                </div>
                
                <div class="filtro">
                    <FormGroup row>
                    {/* <Col md="2">
                        <span>Filtrar pacientes</span>
                    </Col> */}
                    <Col md="12">
                        <InputGroup>
                            <InputGroupAddon addonType="prepend"> 
                                Filtrar pacientes
                            </InputGroupAddon>
                            <Input 
                                value={ this.state.search }
                                onChange={ this.updateSearch.bind(this) }
                                size="lg"
                                placeholder="Buscar por nombre o número de identificacion..."
                            />
                        </InputGroup>
                    </Col>
                    </FormGroup>
                </div>

                <h2 id="subtitulo-pacientes">Seleccione un paciente para comenzar:</h2>
                <FormGroup row>
                    <Col md="12">
                        {afiliados_filtrados.map((item, idx) => (
                            <div class="contenedor-pacientes">
                                <ul class="ul-pacientes" id={item.dip}>
                                    <li class="li-pacientes" onClick={() => this.toggleAccordion(idx)} id="li-pacientes-nombre"><span style={{textTransform: 'capitalize'}}>{(item.primer_apellido + ' ' + item.segundo_apellido + ' ' + item.nombres).toLocaleLowerCase()}</span></li>
                                    <li class="li-pacientes" onClick={() => this.toggleAccordion(idx)} id="li-pacientes-id"><b>Identificación: </b>{item.dip}</li>
                                    <li class="li-pacientes" onClick={() => this.toggleAccordion(idx)} id="li-pacientes-id"><b>Matrícula: </b>{item.matricula}</li>
                                    <li class="li-pacientes" id="li-pacientes-boton"><span onClick={this.toggleModal(item)}>Cargar Datos</span></li>
                                </ul>
                                <Collapse target={item.dip} isOpen={this.state.collapseafiliados[idx]}>
                                    <Card>
                                        <CardBody>
                                        <div class="modal-body">
                                            <p style={{textAlign: "center"}}>
                                                <b>Documento de Identificación Personal: </b>{item.dip}<br/>
                                                <b>Matrícula: </b>{item.matricula}<br/>
                                            </p>
                                            <hr/>
                                            <p>
                                                {item.id_sexo == 'F' ? 
                                                    <span><b>Sexo: </b>Femenino<br/></span>
                                                    :
                                                    <span><b>Sexo: </b>Masculino<br/></span>
                                                }
                                                <b>Fecha de nacimiento: </b>{item.fec_nacimiento}<br/>
                                                <b>Sede: </b><span>{item.sede}</span><br/>
                                                <b>Provincia: </b><span>{item.provincia}</span><br/>
                                                <b>Dirección: </b>{item.direccion}<br/>
                                                <b>Correo: </b>{item.correo}<br/>
                                                <b>Estado civil: </b>{item.estado_civil}
                                                <hr/>
                                                <b>Estado del Estudiante: </b>{item.estado_estudiante}
                                            </p>
                                        </div>
                                        </CardBody>
                                    </Card>
                                </Collapse>
                            </div>
                        ))}
                    </Col>
                </FormGroup>
                
                </CardBody>
            </Card>
            </Col>
            </Row>
          </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    paciente: state.paciente,
    afiliadotelefono: state.afiliadotelefono,
    hc: state.hc,
    historialclinico: state.historialclinico,
    anamnesis: state.anamnesis,
    examenf: state.examenf,
    laboratorios: state.laboratorios,
    antecedentespnp: state.antecedentespnp,
    antecedentespp: state.antecedentespp,
    antecedentesfam: state.antecedentesfam,
    planificacionf: state.planificacionf,
    antecedentesgo: state.antecedentesgo,
    transfusiones: state.transfusiones,
    alergias: state.alergias,
    antecedentesq: state.antecedentesq,
    antecedentesc: state.antecedentesc,
    signosv: state.signosv,
});

const mapActionsToProps = {
    onUpdateUser: updateUser,
    onUpdatePaciente: updatePaciente,
    onUpdateAfiliadoTelefono: updateAfiliadoTelefono,
    onUpdateHC: updateHC,
    onUpdateHistorialClinico: updateHistorialClinico,
    onUpdateAnamnesis: updateAnamnesis,
    onUpdateExamenFisico: updateExamenFisico,
    onUpdateLaboratorios: updateLaboratorios,
    onUpdateAntecedentesPNP: updateAntecedentesPNP,
    onUpdateAntecedentesPP: updateAntecedentesPP,
    onUpdateAntecedentesFam: updateAntecedentesFam,
    onUpdatePlanificacionF: updatePlanificacionF,
    onUpdateAntecedentesGO: updateAntecedentesGO,
    onUpdateTransfusiones: updateTransfusiones,
    onUpdateAlergias: updateAlergias,
    onUpdateAntecedentesQ: updateAntecedentesQ,
    onUpdateAntecedentesC: updateAntecedentesC,
    onUpdateSignosVitales: updateSignosVitales,
}
export default connect(mapStateToProps, mapActionsToProps)(ListaAfiliados);