import { UPDATE_ANTECEDENTESGO } from '../Actions/AntecedentesGOActions';

export default function antecedentesgoReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANTECEDENTESGO:
            return payload.antecedentesgo;
        default:
            return state;
    }
}