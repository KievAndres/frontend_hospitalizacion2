import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// disable ServiceWorker
// import registerServiceWorker from './registerServiceWorker';
import Encabezado from './views/Encabezado/Encabezado';

import { combineReducers, createStore} from 'redux';
import userReducer from './views/Redux/Reducers/UserReducer';
import pacienteReducer from './views/Redux/Reducers/PacienteReducer';
import afiliadotelefonoReducer from './views/Redux/Reducers/AfiliadoTelefonoReducer';
import hcReducer from './views/Redux/Reducers/TieneHCReducer';
import historialclinicoReducer from './views/Redux/Reducers/HistorialClinicoReducer';
import anamnesisReducer from './views/Redux/Reducers/AnamnesisReducers';
import examenfisicoReducer from './views/Redux/Reducers/ExamenFisicoReducer';
import laboratoriosReducer from './views/Redux/Reducers/LaboratoriosReducer';
import antecedentespnpReducer from './views/Redux/Reducers/AntecedentesPNPReducer';
import antecedentesppReducer from './views/Redux/Reducers/AntecedentesPPReducer';
import antecedentesfamReducer from './views/Redux/Reducers/AntecedentesFamiliaresReducer';
import planificacionfReducer from './views/Redux/Reducers/PlanificacionFReducer';
import antecedentesgoReducer from './views/Redux/Reducers/AntecedentesGOReducer';
import transfusionesReducer from './views/Redux/Reducers/TransfusionesReducer';
import alergiasReducer from './views/Redux/Reducers/AlergiasReducer';
import antecedentesqReducer from './views/Redux/Reducers/AntecedentesQReducer';
import antecedentescReducer from './views/Redux/Reducers/AntecedentesCReducer';
import signosvitalesReducer from './views/Redux/Reducers/SignosVitalesReducer';

import { Provider } from 'react-redux';


const allReducers = combineReducers({
    user: userReducer,
    paciente: pacienteReducer,
    afiliadotelefono: afiliadotelefonoReducer,
    hc: hcReducer,
    historialclinico: historialclinicoReducer,
    anamnesis: anamnesisReducer,
    examenf: examenfisicoReducer,
    laboratorios:  laboratoriosReducer,  
    antecedentespnp: antecedentespnpReducer, 
    antecedentespp: antecedentesppReducer,
    antecedentesfam: antecedentesfamReducer,
    planificacionf: planificacionfReducer,
    antecedentesgo: antecedentesgoReducer,
    transfusiones: transfusionesReducer,
    alergias: alergiasReducer,
    antecedentesq: antecedentesqReducer,
    antecedentesc: antecedentescReducer,
    signosv: signosvitalesReducer,
})

const store = createStore(
    allReducers,
    {
        user: 'Indefinido',
        paciente: 'Sin asignar',
        afiliadotelefono: 'Sin telefono',
        hc: false,
        historialclinico: 'Sin historial',
        anamnesis: 'Sin anamnesis',
        examenf: 'Sin Examen Físico',
        laboratorios: 'Sin laboratorios',
        antecedentespnp: 'Sin antecedentes PNP',
        antecedentespp: 'Sin antecedentes PP',
        antecedentesfam: 'Sin antecedentes Familiares',
        planificacionf: 'Sin planificacion familiar',
        antecedentesgo: 'Sin antecedentes gineco obstetricos',
        transfusiones: 'Sin tranfusiones',
        alergias: 'Sin alergias',
        antecedentesq: 'Sin antecedentes quirúrgicos',
        antecedentesc: 'Sin antecedentes clínicos',
        signosv: 'Sin asignar',
    },
    window.devToolsExtension && window.devToolsExtension()
);

console.log(store.getState());

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('root'));
// disable ServiceWorker
// registerServiceWorker();
