import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, Button,Collapse } from 'reactstrap';

class DatosHistorial extends Component {
  constructor(props){
    super(props);
    this.state = {
      collapse: false,
      collapse: false,

      fadeIn: true,
      timeout: 300,
      estudiante: [],
      idA:'',
    };

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
  }

  toggle(){
    if(this.state.collapse==false){
      this.setState({
        collapse: true,
        collapse: false,
      });
    }else if(this.state.collapse2==false){
      this.setState({
        collapse: false,
        collapse2: true,
      });
    }
  }

  toggleFade(){
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  render(){
    const lista = this.props.list.map((item, indice) =>{
      return(
        <tr key={item.id}>
          <td>{ item.nombre }</td>
        </tr>
      )
    })

    return(
      <div className="animated fadeIn">
    <Row>
          <Col xs="12">
            <Card>
              
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Datos Personales</strong>
              </CardHeader>
    
              <CardBody>
          
                  <div>
                  <Table responsive hover >
                        <thead>
                        <tr>                        
                          <th>Nombre</th>                                                                      
                        </tr>
                        </thead>
                        <tbody>
                                {lista}
                        </tbody>
                  </Table>


                  </div>
          
                  {/* { this.props.muestraBotonPrimeraConsulta ? <BotonPrimeraConsulta idA={lista[0].key} /> : null }
                  { this.props.muestraBotonTratamiento ? <BotonTratamiento /> : null }
                  <br/>
                  { this.props.muestraBotonHistorial ? <BotonHistorial idA={lista[0].key}/> : null } */}
                  
              </CardBody>
            </Card>
          </Col>
        </Row>                 
    </div>
    );
  }
}

export default DatosHistorial;