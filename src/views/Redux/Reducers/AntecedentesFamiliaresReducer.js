import { UPDATE_ANTECEDENTESFAM } from '../Actions/AntecedentesFamiliaresActions';

export default function antecedentesfamReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANTECEDENTESFAM:
            return payload.antecedentesfam;
        default:
            return state;
    }
}