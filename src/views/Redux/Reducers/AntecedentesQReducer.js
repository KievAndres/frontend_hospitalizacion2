import { UPDATE_ANTECEDENTESQ } from '../Actions/AntecedentesQActions';

export default function antecedentesqReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANTECEDENTESQ:
            return payload.antecedentesq;
        default:
            return state;
    }
}