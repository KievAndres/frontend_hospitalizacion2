import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
import Search from 'react-search-box';
import Boton from "./Boton.js";
import Widget04 from './Widget04';
import Anamnesis from './Anamnesis'


class NotaInternacionVista extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            afiliados: [],
            anamnesis: [],
            primary: false,
            Nombre:'',
            Direccion:'',
            Telefono: '',
            Fecha:'',
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',

            search: '',
            data: [], //Esto es para el buscador
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/internados/');
            const internados = await respuesta.json();
            console.log(internados)
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }

        try{
            const respuesta = await fetch('http://127.0.0.1:8000/afiliados/');
            const afiliados = await respuesta.json();
            console.log(afiliados)
            this.setState({
                afiliados
            });
        } catch(e){
            console.log(e);
        }

        try{
            const respuesta = await fetch('http://127.0.0.1:8000/anamnesis/');
            const anamnesis = await respuesta.json();
            console.log(anamnesis)
            this.setState({
                anamnesis
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange(value){    
      console.log(value);
      this.setState({ muestraBoton: true });
    }

    handleOnSubmit(e){
      console.log(this.state.Telefono)
      alert(
        'Hola: ' + this.state.Nombre + 
        '\nDireccion: ' + this.state.Direccion + 
        '\nTelefono: ' + this.state.Telefono +
        '\nFecha: ' + this.state.Fecha +
        '\nHora: ' + this.state.Hora +
        '\nDescripcion ' + this.state.Descripcion +
        '\nDiagnostico ' + this.state.Diagnostico +
        '\nIndicaciones ' + this.state.Indicaciones
      );
      e.preventDefault();

      let url =''
      let data = {}

      url = 'http://127.0.0.1:8000/internadosas/'
      data = {
          nombre: this.state.Nombre,
          direccion: this.state.Direccion,
          telefono: this.state.Telefono,
          fecha: this.state.Fecha,
          hora: this.state.Hora,
          descripcion: this.state.Descripcion,
          diagnostico: this.state.Diagnostico,
          indicaciones: this.state.Indicaciones
      }

      try{
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Succes:', response));
      } catch(e){
        console.log(e)
      }

      this.setState({
        primary: !this.state.primary,
      });
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    updateSearch(event){
        this.setState({search: event.target.value.substr(0,20)});
    }

    render() {
        let historiales_filtrados = this.state.internados.filter(
            (item) => {
                return item.diagnostico.toUpperCase().indexOf(
                  this.state.search.toUpperCase()) !== -1; 
            }
        );
        return (
            <div className="animated fadeIn">
            <h1>Historial de Internacion</h1>
            <br/><br/>
                <Row>
                <Col sm="6" md="2">
                    <Widget04 icon="icon-people" color="info" header={ this.state.internados.length } value="75">Internados</Widget04>
                </Col>  

                <Col md="1">      
                    <h1>
                    Filtro
                    </h1>
                </Col>

                <h1>
                    <Col md="5">
                        <ul>
                            <input type="text"
                            value={ this.state.search }
                            onChange={ this.updateSearch.bind(this) }
                            />
                        </ul>
                    </Col>
                </h1>

                <Col sm="6" md="2">
                    <Widget04 icon="icon-people" color="success" header={ historiales_filtrados.length } value="75" invert>Filtrados</Widget04>
                </Col> 
                    {/* { this.state.muestraBoton ? <Boton />: null } */}

                    <br/>
                

                <Col xs="12" md="12">
                {historiales_filtrados.map(item => (
                  <div key={item.id}>

                <Card>
                    <CardHeader>
                    <strong><h1>{item.nombre}</h1></strong> <h4>Nota Internacion - PROMES </h4> 
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Número telefónico</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.telefono}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Diagnóstico</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.diagnostico}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Indicaciones</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.indicaciones}</h5>
                        </Col>
                        </FormGroup>
                        
                        
                    </Form>

                        <Anamnesis id={item.id}/>

                    </CardBody>

                    

                    <CardFooter>
                    <h4>Fecha de Atención: {item.fecha}</h4>
                    <h5>Hora de Atención: {item.hora}</h5>
                    </CardFooter>
                </Card>
                </div>    
                ))}
                </Col>
            </Row>
        </div>
        );
    }
}
export default NotaInternacionVista;