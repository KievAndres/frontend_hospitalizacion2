import { UPDATE_HISTORIALCLINICO } from '../Actions/HistorialClinicoActions';

export default function historialclinicoReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_HISTORIALCLINICO:
            return payload.historialclinico;
        default:
            return state;
    }
}