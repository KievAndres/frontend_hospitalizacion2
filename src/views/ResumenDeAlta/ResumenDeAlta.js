import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
import Encabezado from '../Encabezado/Encabezado';
import EncabezadoDatos from '../EncabezadoDatos/EncabezadoDatos';

class ResumenDeAlta extends Component {
    constructor(props){
        super(props)

        this.state = {
        }
    }

   render() {

        return (
            <div className="animated fadeIn" class="historiaclinica">
            <Row>
                <Col md="12">
                    <Card>
                        <CardBody>
                            <Encabezado />
                            <EncabezadoDatos titulo="RESUMEN DE ALTA"/>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            <h1>Resumen de Alta</h1>
            <br/><br/>
                <Row>
            <Col xs="12" md="12">

                <Card>
                    <CardHeader>
                    <strong><h1>123</h1></strong> <h4>RESUMEN DE ALTA - SSU </h4> 
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Fecha de Ingreso</strong> </Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>123</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Hora de Ingreso</strong> </Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>1</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Fecha de Egreso</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="date" 
                            name="Direccion" 
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Servicio</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="text" 
                            name="Direccion" 
                            placeholder="Ej. Cirugía"
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>


                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Diagnóstico de Ingreso</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h6>1</h6>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Diagnóstico de Egreso</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Diágnostico con el que sale el paciente del SSU"
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Tratamiento</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h6>1</h6>
                        </Col>
                        </FormGroup>
                        
                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Antecedents, Anamnesis y Examen Físico</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Antecedentes médicos"
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Hallazgos significativos de Laboratorio</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Laboratorios realizados"
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Evolución intrahospitalaria</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Mejorías observadas desde la última intervención quirúrgica"
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Condiciones de alta, tratamiento y control post hospitalario</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion"
                            placeholder="Condición del paciente" 
                            // onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>   
                </Col>
            </Row>
        </div>
        );
    }
}
export default ResumenDeAlta;