import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import Encabezado from '../Encabezado/Encabezado';
import EncabezadoDatos from '../EncabezadoDatos/EncabezadoDatos';
import ReactTimeout from 'react-timeout';

class HistoriaClinica extends Component {
    constructor(props){
        super(props)
        
        var hoy = new Date(),
            f = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

        this.state = {
            afiliadox: [],
            fecha: f,
            hora: new Date(),
            id_historiac: 'hc'+this.props.match.params.dip,
            consulta: '',
            historia: '',
            datosc: '',
            diagnostico: '',
            id_anamnesis: 'an'+this.props.match.params.dip,
            fecha_actualizacion: f,
            vivienda: 'Propia',
            shi: 'Si',
            aguai: 'Si',
            alimentacion: 'Dieta variada sin predominancia de grupo alimenticio',
            alcohol: 'No Consume',
            afrecuencia: '',
            aobservaciones: '',
            tabaco: 'No Consume',
            tfrecuencia: '',
            tobservaciones: '',
            somnia: 8,
            diuresis: 3,
            hintestinal: 1,
            id_app: 'app'+this.props.match.params.dip,
            combe: false,
            hipertension: false,
            tbcpulmonar: false,
            diabetes: false,
            pmamaria: false,
            cardiopatias: false,
            ets: false,
            varices: false,
            erenal: false,
            hepatopatias: false,
            tgenitales: false,
            egastrointestinal: false,
            otros: '',
            aquirurgicos: '',
            aquirurgicosano: 2017,
            alergias: '',
            transfusionesano: 2017,
            transfusionesdes: '',
            hipertensionf: false,
            tbcpulmonarf: false,
            diabetesf: false,
            pmamariaf: false,
            cardiopatiasf: false,
            etsf: false,
            varicesf: false,
            erenalf: false,
            hepatopatiasf: false,
            tgenitalesf: false,
            egastrointestinalf: false,
            otrosf: '',
            observacionesf: '',
            gesta: '',
            menarca: '',
            para: '',
            fum: '',
            abortos: '',
            catamenio: '',
            irs: '',

        }
        this.cambiaEstado = this.cambiaEstado.bind(this);
        this.enviarInformacion = this.enviarInformacion.bind(this);
        this.enviarInformacionPromise = this.enviarInformacionPromise.bind(this);
        this.cambiaEstadoCheckBox = this.cambiaEstadoCheckBox.bind(this);
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/api/v1/afiliado/'+this.props.match.params.dip+'/');
            const afiliadox = await respuesta.json();
            console.log(afiliadox);
            this.setState({
                afiliadox
            });
            
        } catch(e){
            console.log(e);
        }
    }

    enviarInformacionPromise(e){
        e.preventDefault();
        
        // Historia Clínica
        let urlhc=''
        let datahc={}

        urlhc='http://localhost:8000/api/v1/historiaclinica/'
        datahc={
            id_historiaclinica: this.state.id_historiac,
            fecha_primeraconsulta: this.state.fecha,
            hora_primeraconsulta: this.state.hora.toLocaleTimeString(),
            afiliado: this.props.match.params.dip,
            motivo_consulta: this.state.consulta,
            historia_enfermedad_actual: this.state.historia,
            datos_complementarios: this.state.datosc,
            diagnostico_presuntivo: this.state.diagnostico,
        }
        try{
            fetch(urlhc, {
                method: 'POST',
                body: JSON.stringify(datahc),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('HistoriaClinica:', response))
            .then(() =>{

                //Anamnesis

                let urlan=''
                let dataan={}
        
                urlan='http://localhost:8000/api/v1/anamnesis/'
                dataan={
                    id_anamnesis: this.state.id_anamnesis,
                    fecha_actualizacion: this.state.fecha_actualizacion,
                    historia_clinica: this.state.id_historiac
                }
                try{
                    fetch(urlan, {
                        method: 'POST',
                        body: JSON.stringify(dataan),
                        headers:{
                            'Content-Type': 'application/json'
                        }
                    }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(response => console.log('Anamnesis:', response))
                    .then(() =>{
                        // Antecedentes Personales no patologicos

                        let urlapnp=''
                        let dataapnp={}

                        urlapnp='http://localhost:8000/api/v1/antecedentespnp/'
                        dataapnp={
                            vivienda: this.state.vivienda,
                            servicio_higienico_intradomiciliario: this.state.shi,
                            agua_intradomiciliaria: this.state.aguai,
                            alimentacion: this.state.alimentacion,
                            habito_tabaquico: this.state.tabaco,
                            habito_alcoholico: this.state.alcohol,
                            somnia: this.state.somnia + ' horas al dia',
                            diuresis: this.state.diuresis + ' veces al dia',
                            habito_intestinal: this.state.hintestinal + ' veces al dia',
                            anamnesis: this.state.id_anamnesis,
                        }
                        try{
                            fetch(urlapnp, {
                                method: 'POST',
                                body: JSON.stringify(dataapnp),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.log('Error:', error))
                            .then(response => console.log('APNP:', response));
                        }catch(e){
                            console.log(e)
                        }

                        // Antecedentes Familiares
                        let urlaf=''
                        let dataaf={}
                        urlaf='http://localhost:8000/api/v1/antecedentesf/'
                        dataaf={
                            hipertensionf: this.state.hipertensionf,
                            tbcpulmonarf: this.state.tbcpulmonarf,
                            diabetesf: this.state.diabetesf,
                            pmamariaf: this.state.pmamariaf,
                            cardiopatiasf: this.state.cardiopatiasf,
                            etsf: this.state.etsf,
                            varicesf: this.state.varicesf,
                            erenalf: this.state.erenalf,
                            hepatopatiasf: this.state.hepatopatiasf,
                            tgenitalesf: this.state.tgenitalesf,
                            egastrointestinalf: this.state.egastrointestinalf,
                            otrosf: this.state.otrosf,
                            observacionesf: this.state.observacionesf,
                            anamnesis: this.state.id_anamnesis
                        }
                        try{
                            fetch(urlaf,{
                                method: 'POST',
                                body: JSON.stringify(dataaf),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.error('Error:', error))
                            .then(response => console.log('AFamiliares:', response));
                        }catch(e){
                            console.log(e);
                        }

                        // Antecedentes Gineco Obstetricos
                        let urlago=''
                        let dataago={}

                        urlago='http://localhost:8000/api/v1/antecedentesgo/'
                        dataago={
                            gesta: this.state.gesta,
                            menarca: this.state.menarca,
                            para: this.state.para,
                            fum: this.state.fum,
                            abortos: this.state.abortos,
                            catamenio: this.state.catamenio,
                            irs: this.state.irs,
                            anamnesis: this.state.id_anamnesis
                        }
                        try{
                            fetch(urlago,{
                                method: 'POST',
                                body: JSON.stringify(dataago),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.error('Error:',error))
                            .then(response => console.log('AntecedentesGO:',response));
                        }catch(e){
                            console.log(e);
                        }

                        //Antecedentes Personales Patologicos

                        let urlapp=''
                        let dataapp={}

                        urlapp='http://localhost:8000/api/v1/antecedentespp/'
                        dataapp={
                            id_app: this.state.id_app,
                            combe: this.state.combe,
                            anamnesis: this.state.id_anamnesis,
                        }
                        try{
                            fetch(urlapp, {
                                method: 'POST',
                                body: JSON.stringify(dataapp),
                                headers:{
                                    'Content-Type': 'application/json'
                                }
                            }).then(res => res.json())
                            .catch(error => console.error('Error:', error))
                            .then(response => console.log('APP:', response)).
                            then(() => {
                                // Antecedentes Clinicos
                                let urlac=''
                                let dataac={}

                                urlac='http://localhost:8000/api/v1/antecedentesc/'
                                dataac={
                                    hipertension: this.state.hipertension,
                                    tbcpulmonar: this.state.tbcpulmonar,
                                    diabetes: this.state.diabetes,
                                    pmamaria: this.state.pmamaria,
                                    cardiopatias: this.state.pmamaria,
                                    ets: this.state.ets,
                                    varices: this.state.varices,
                                    erenal: this.state.erenal,
                                    hepatopatias: this.state.hepatopatias,
                                    tgenitales: this.state.tgenitales,
                                    egastrointestinal: this.state.egastrointestinal,
                                    otros: this.state.otros,
                                    app: this.state.id_app,
                                }
                                try{
                                    fetch(urlac,{
                                        method: 'POST',
                                        body: JSON.stringify(dataac),
                                        headers:{
                                            'Content-Type': 'application/json'
                                        }
                                    }).then(res => res.json())
                                    .catch(error => console.error('Error:', error))
                                    .then(response => console.log('AClinicos:', response));
                                }catch(e){
                                    console.log(e)
                                }

                                // Antecedentes Quirurgicos
                                let urlaq = ''
                                let dataaq = {}

                                urlaq='http://localhost:8000/api/v1/antecedentesq/'
                                dataaq={
                                    ano: this.state.aquirurgicosano,
                                    tratamiento_quirurgico: this.state.aquirurgicos,
                                    app: this.state.id_app
                                }
                                try{
                                    fetch(urlaq, {
                                        method: 'POST',
                                        body: JSON.stringify(dataaq),
                                        headers:{
                                            'Content-Type': 'application/json'
                                        }
                                    }).then(res => res.json())
                                    .catch(error => console.error('Error:', error))
                                    .then(response => console.log('AQuirurgicos:', response));
                                }catch(e){
                                    console.log(e);
                                }

                                // Alergias
                                let urlal=''
                                let dataal={}
                                
                                urlal='http://localhost:8000/api/v1/alergias/'
                                dataal={
                                    alergia: this.state.alergias,
                                    app: this.state.id_app
                                }
                                try{
                                    fetch(urlal,{
                                        method: 'POST',
                                        body: JSON.stringify(dataal),
                                        headers:{
                                            'Content-Type': 'application/json'
                                        }
                                    }).then(res => res.json())
                                    .catch(error => console.error('Error:', error))
                                    .then(response => console.log('Alergias:', response))
                                }catch(e){
                                    console.log(e);
                                }

                                //Transfusiones
                                let urltr=''
                                let datatr={}
                                urltr='http://localhost:8000/api/v1/transfusiones/'
                                datatr={
                                    ano: this.state.transfusionesano,
                                    descripcion: this.state.transfusionesdes,
                                    app: this.state.id_app
                                }
                                try{
                                    fetch(urltr,{
                                        method: 'POST',
                                        body: JSON.stringify(datatr),
                                        headers:{
                                            'Content-Type': 'application/json'
                                        }
                                    }).then(res => res.json())
                                    .catch(error => console.error('Error', error))
                                    .then(response => console.log('Transfusiones', response));
                                }catch(e){
                                    console.log(e);
                                }
                            });
                        }catch(e){
                            console.log(e)
                        }
                    });
                }catch(e){
                    console.log(e)
                } 
            });
        }catch(e){
            console.log(e)
        }

        this.setState({
            hora: new Date()
        });
    }

    cambiaEstado(event){
        this.setState({vivienda: event.target.value});
    }

    enviarInformacion(e){
        e.preventDefault();
        
        // Historia Clínica
        let urlhc=''
        let datahc={}

        urlhc='http://localhost:8000/api/v1/historiaclinica/'
        datahc={
            id_historiaclinica: this.state.id_historiac,
            fecha_primeraconsulta: this.state.fecha,
            hora_primeraconsulta: this.state.hora.toLocaleTimeString(),
            afiliado: this.props.match.params.dip,
            motivo_consulta: this.state.consulta,
            historia_enfermedad_actual: this.state.historia,
            datos_complementarios: this.state.datosc,
            diagnostico_presuntivo: this.state.diagnostico,
        }
        try{
            fetch(urlhc, {
                method: 'POST',
                body: JSON.stringify(datahc),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response));
        }catch(e){
            console.log(e)
        }

        this.setState({
            hora: new Date()
        });

        // ANAMNESIS
        setTimeout(() => {
            let urlan=''
            let dataan={}
    
            urlan='http://localhost:8000/api/v1/anamnesis/'
            dataan={
                id_anamnesis: this.state.id_anamnesis,
                fecha_actualizacion: this.state.fecha_actualizacion,
                historia_clinica: this.state.id_historiac
            }
            try{
                fetch(urlan, {
                    method: 'POST',
                    body: JSON.stringify(dataan),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e)
            }   
        }, 600);
        
        // ANTECEDENTES PERSONALES NO PATOLOGICOS
        setTimeout(() => {
            let urlapnp=''
            let dataapnp={}

            urlapnp='http://localhost:8000/api/v1/antecedentespnp/'
            dataapnp={
                vivienda: this.state.vivienda,
                servicio_higienico_intradomiciliario: this.state.shi,
                agua_intradomiciliaria: this.state.aguai,
                alimentacion: this.state.alimentacion,
                habito_tabaquico: this.state.tabaco,
                habito_alcoholico: this.state.alcohol,
                somnia: this.state.somnia + ' horas al dia',
                diuresis: this.state.diuresis + ' veces al dia',
                habito_intestinal: this.state.hintestinal + ' veces al dia',
                anamnesis: this.state.id_anamnesis,
            }
            try{
                fetch(urlapnp, {
                    method: 'POST',
                    body: JSON.stringify(dataapnp),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.log('Error:', error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e)
            }


            //Antecedentes Personales Patologicos

            let urlapp=''
            let dataapp={}

            urlapp='http://localhost:8000/api/v1/antecedentespp/'
            dataapp={
                id_app: this.state.id_app,
                combe: this.state.combe,
                anamnesis: this.state.id_anamnesis,
            }
            try{
                fetch(urlapp, {
                    method: 'POST',
                    body: JSON.stringify(dataapp),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e)
            }

            // Antecedentes Familiares
            let urlaf=''
            let dataaf={}
            urlaf='http://localhost:8000/api/v1/antecedentesf/'
            dataaf={
                hipertensionf: this.state.hipertensionf,
                tbcpulmonarf: this.state.tbcpulmonarf,
                diabetesf: this.state.diabetesf,
                pmamariaf: this.state.pmamariaf,
                cardiopatiasf: this.state.cardiopatiasf,
                etsf: this.state.etsf,
                varicesf: this.state.varicesf,
                erenalf: this.state.erenalf,
                hepatopatiasf: this.state.hepatopatiasf,
                tgenitalesf: this.state.tgenitalesf,
                egastrointestinalf: this.state.egastrointestinalf,
                otrosf: this.state.otrosf,
                observacionesf: this.state.observacionesf,
                anamnesis: this.state.id_anamnesis
            }
            try{
                fetch(urlaf,{
                    method: 'POST',
                    body: JSON.stringify(dataaf),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success', response));
            }catch(e){
                console.log(e);
            }

            // Antecedentes Gineco Obstetricos
            let urlago=''
            let dataago={}

            urlago='http://localhost:8000/api/v1/antecedentesgo/'
            dataago={
                gesta: this.state.gesta,
                menarca: this.state.menarca,
                para: this.state.para,
                fum: this.state.fum,
                abortos: this.state.abortos,
                catamenio: this.state.catamenio,
                irs: this.state.irs,
                anamnesis: this.state.id_anamnesis
            }
            try{
                fetch(urlago,{
                    method: 'POST',
                    body: JSON.stringify(dataago),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:',error))
                .then(response => console.log('Success:',response));
            }catch(e){
                console.log(e);
            }
        }, 1000);

        setTimeout(() =>{
            // Antecedentes Clínicos
            let urlac=''
            let dataac={}

            urlac='http://localhost:8000/api/v1/antecedentesc/'
            dataac={
                hipertension: this.state.hipertension,
                tbcpulmonar: this.state.tbcpulmonar,
                diabetes: this.state.diabetes,
                pmamaria: this.state.pmamaria,
                cardiopatias: this.state.pmamaria,
                ets: this.state.ets,
                varices: this.state.varices,
                erenal: this.state.erenal,
                hepatopatias: this.state.hepatopatias,
                tgenitales: this.state.tgenitales,
                egastrointestinal: this.state.egastrointestinal,
                otros: this.state.otros,
                app: this.state.id_app,
            }
            try{
                fetch(urlac,{
                    method: 'POST',
                    body: JSON.stringify(dataac),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e)
            }

            // Antecedentes Quirurgicos
            let urlaq = ''
            let dataaq = {}

            urlaq='http://localhost:8000/api/v1/antecedentesq/'
            dataaq={
                ano: this.state.aquirurgicosano,
                tratamiento_quirurgico: this.state.aquirurgicos,
                app: this.state.id_app
            }
            try{
                fetch(urlaq, {
                    method: 'POST',
                    body: JSON.stringify(dataaq),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e);
            }

            // Alergias
            let urlal=''
            let dataal={}
            
            urlal='http://localhost:8000/api/v1/alergias/'
            dataal={
                alergia: this.state.alergias,
                app: this.state.id_app
            }
            try{
                fetch(urlal,{
                    method: 'POST',
                    body: JSON.stringify(dataal),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response))
            }catch(e){
                console.log(e);
            }

            //Transfusiones
            let urltr=''
            let datatr={}
            urltr='http://localhost:8000/api/v1/transfusiones/'
            datatr={
                ano: this.state.transfusionesano,
                descripcion: this.state.transfusionesdes,
                app: this.state.id_app
            }
            try{
                fetch(urltr,{
                    method: 'POST',
                    body: JSON.stringify(datatr),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error', error))
                .then(response => console.log('Success', response));
            }catch(e){
                console.log(e);
            }
        },1200);
    }
    prueba1 = (e) => {
        e.preventDefault();
        console.log(this.state.consulta);
        console.log(this.state.historia);
        console.log(this.state.datosc);
        console.log(this.state.diagnostico);
        console.log(this.state.vivienda);
        console.log(this.state.shi);
        console.log(this.state.aguai);
        console.log(this.state.alimentacion);
        console.log(this.state.alcohol);
        console.log(this.state.afrecuencia);
        console.log(this.state.aobservaciones);
        console.log(this.state.tabaco);
        console.log(this.state.tfrecuencia);
        console.log(this.state.tobservaciones);
        console.log(this.state.somnia);
        console.log(this.state.diuresis);
        console.log(this.state.hintestinal);
        console.log(this.state.id_historiac);
        console.log(this.state.hora.toLocaleTimeString());
        console.log(this.state.id_anamnesis);
        console.log(this.state.fecha_actualizacion);
        console.log(this.state.id_historiac);
        this.setState({
            hora: new Date()
        });
        console.log(this.state.hipertension);
        console.log(this.state.tbcpulmonar);
        console.log(this.state.diabetes);
        console.log(this.state.pmamaria);
        console.log(this.state.cardiopatias);
        console.log(this.state.ets);
        console.log(this.state.varices);
        console.log(this.state.erenal);
        console.log(this.state.hepatopatias);
        console.log(this.state.tgenitales);
        console.log(this.state.egastrointestinal);
        console.log(this.state.otros);
    }
    cambiaEstadoCheckBox(event){
        const target = event.target;
        const value = target.type === 'checkbox' ?
        target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        return (
          <div className="animated fadeIn">
            <Encabezado />
            {/* <EncabezadoDatos
                dip={this.state.afiliadox.dip} 
                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                matricula={this.state.afiliadox.matricula}
                sexo={this.state.afiliadox.id_sexo}
                edad={this.state.afiliadox.fec_nacimiento}
                piso='Sin Asignar'
                sala='Sin Asignar'
                cama='Sin Asignar'
                servicio='Sin Asignar'
            /> */}
            <Row>
                {/* <Col xs="0" md="2">
                </Col> */}
                <Col md="12">
                <Card>
                    <CardHeader>
                    
                        <center><h2><b>HISTORIA CLÍNICA</b></h2></center>         
                    </CardHeader>
                    <CardBody>
                    <h5><b>Fecha Primera visita: </b>{this.state.fecha} </h5>
                    <h4><b>1. Filiación</b></h4>
                    <Table responsive borderless>
                        <tbody>
                        <tr>
                            <td><b>Nombre: </b>{this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}</td>
                            <td><b>Matrícula: </b>{this.state.afiliadox.matricula}</td>
                        </tr>
                        <tr>
                            <td><b>Sexo: </b>{this.state.afiliadox.id_sexo}</td>
                            <td><b>Edad: </b>{this.state.afiliadox.fec_nacimiento}</td>
                        </tr>
                        <tr>
                            <td><b>Procedencia: </b>{this.state.afiliadox.sede}</td>
                            <td><b>Residencia: </b>{this.state.afiliadox.sede}</td>
                        </tr>
                        <tr>
                            <td><b>Estado civil: </b>{this.state.afiliadox.estado_civil}</td>
                            <td><b>Domicilio: </b>{this.state.afiliadox.direccion}</td>
                        </tr>
                        </tbody>

                        {/* FALTA AÑADIR TELEFONOS */}
                    </Table> 
                    <FormGroup row>
                        <Col md="3">
                            <h5>Motivo Consulta</h5>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="text"
                                placeholder="Motivo de la consulta"
                                onChange={e => this.setState({consulta: e.target.value})}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <h5>Historia de la Enfermedad Actual</h5>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea"
                                placeholder="Historia de la enfermedad actual"
                                onChange={e => this.setState({historia: e.target.value})}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <h5>Datos Complementarios</h5>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea"
                                placeholder="Datos Complementarios"
                                onChange={e => this.setState({datosc: e.target.value})}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="3">
                            <h5>Diagnóstico Presuntivo</h5>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea"
                                placeholder="Diagnóstico Presuntivo"
                                onChange={e => this.setState({diagnostico: e.target.value})}
                            />
                        </Col>
                    </FormGroup>
                    <center><h5><b>ANAMNESIS</b></h5></center>
                    <hr/>
                    <h4>ANTECEDENTES PERSONALES NO PATOLOGICOS</h4>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Vivienda</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios1" value="Propia" checked={this.state.vivienda==='Propia'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio1">Propia</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios1" value="Alquilada" checked={this.state.vivienda==='Alquilada'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio2">Alquilada</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio3" name="inline-radios1" value="Cedida" checked={this.state.vivienda==='Cedida'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio3">Cedida</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio4" name="inline-radios1" value="Anticretico" checked={this.state.vivienda==='Anticretico'} onChange={this.cambiaEstado}/>
                                <Label check className="form-check-label" htmlFor="inline-radio4">Anticrético</Label>
                            </FormGroup>
                        </Col>
                    </FormGroup>
                    <h4>SERVICIOS BASICOS</h4>
                    <FormGroup row>
                        <Col md="4">
                            <Label>Servicio Higienico Intradomiciliario</Label>
                        </Col>
                        <Col md="2">
                            <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio11" name="inline-radios2" value="Si" checked={this.state.shi==='Si'} onChange={e => this.setState({shi: e.target.value})} />
                                <Label check className="form-check-label" htmlFor="inline-radio11">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio12" name="inline-radios2" value="No" checked={this.state.shi==='No'} onChange={e => this.setState({shi: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio12">No</Label>
                            </FormGroup>
                        </Col>

                        <Col md="4">
                            <Label>Agua Intradomiciliaria</Label>
                        </Col>
                        <Col md="2">
                        <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio21" name="inline-radios3" value="Si" checked={this.state.aguai==='Si'} onChange={e => this.setState({aguai: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio21">Si</Label>
                            </FormGroup>
                            <FormGroup check inline>
                            <Input className="form-check-input" type="radio" id="inline-radio22" name="inline-radios3" value="No" checked={this.state.aguai==='No'} onChange={e => this.setState({aguai: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio22">No</Label>
                            </FormGroup>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="3">
                            <h5>Alimentación</h5>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="text"
                                defaultValue={this.state.alimentacion}
                                onChange={e => this.setState({alimentacion: e.target.value})}
                            />
                        </Col>
                    </FormGroup>

                    <h4>HABITOS</h4>
                    <FormGroup row>
                        <Col md="3">
                            <Label>Habito alcoholico</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio31" name="inline-radios4" value="Consume" checked={this.state.alcohol==='Consume'} onChange={e => this.setState({alcohol: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio31">Consume</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio32" name="inline-radios4" value="No Consume" checked={this.state.alcohol==='No Consume'} onChange={e => this.setState({alcohol: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio32">No Consume</Label>
                            </FormGroup>
                        
                            <br/><br/>

                            <FormGroup row>
                                <Col md="3">
                                    Frecuencia
                                </Col>
                                <Col >
                                    <Input 
                                        type="text"
                                        placeholder="Frecuencia"
                                        onChange={e => this.setState({afrecuencia: e.target.value})}
                                    />
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Col md="3">
                                    Observaciones
                                </Col>
                                <Col >
                                    <Input 
                                        type="textarea"
                                        placeholder="Observaciones"
                                        onChange={e => this.setState({aobservaciones: e.target.value})}
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="3">
                            <Label>Habito tabáquico</Label>
                        </Col>
                        <Col md="9">
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio41" name="inline-radios5" value="Consume" checked={this.state.tabaco==='Consume'} onChange={e => this.setState({tabaco: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio41">Consume</Label>
                            </FormGroup>
                            <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="inline-radio42" name="inline-radios5" value="No Consume" checked={this.state.tabaco==='No Consume'} onChange={e => this.setState({tabaco: e.target.value})}/>
                                <Label check className="form-check-label" htmlFor="inline-radio42">No Consume</Label>
                            </FormGroup>
                        
                            <br/><br/>

                            <FormGroup row>
                                <Col md="3">
                                    Frecuencia
                                </Col>
                                <Col >
                                    <Input 
                                        type="text"
                                        placeholder="Frecuencia"
                                        onChange={e => this.setState({tfrecuencia: e.target.value})}
                                    />
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Col md="3">
                                    Observaciones
                                </Col>
                                <Col >
                                    <Input 
                                        type="textarea"
                                        placeholder="Observaciones"
                                        onChange={e => this.setState({tobservaciones: e.target.value})}
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="2">
                            <h5>Somnia</h5>
                        </Col>
                        <Col md="2">
                            <Input 
                                type="number"
                                defaultValue={this.state.somnia}
                                onChange={e => this.setState({somnia: e.target.value})}
                            />
                        </Col>
                        <Col md="2">
                            <Label>horas al día</Label>
                        </Col>
                        
                    </FormGroup>

                    <FormGroup row>
                        <Col md="2">
                            <h5>Diuresis</h5>
                            </Col>
                        <Col md="2">
                            <Input 
                                type="number"
                                defaultValue={this.state.diuresis}
                                onChange={e => this.setState({diuresis: e.target.value})}
                            />
                        </Col>
                        <Col md="2">
                            <Label>veces al día</Label>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="2">
                            <h5>Hábito intestinal</h5>
                        </Col>
                        <Col md="2">
                            <Input 
                                type="number"
                                defaultValue={this.state.hintestinal}
                                onChange={e => this.setState({hintestinal: e.target.value})}
                            />
                        </Col>
                        <Col md="2">
                            <Label>veces al día</Label>
                        </Col>
                    </FormGroup>


                    <h4>ANTECEDENTES PERSONALES PATOLÓGICOS</h4>

                    <FormGroup row>
                        <Col md="1">
                            <FormGroup check classNam="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox0" name="combe" />
                                <Label check className="form-check-label" htmlFor="checkbox0">COMBE</Label>
                            </FormGroup>
                        </Col>
                        <Col md="11">
                                <Input
                                    type="text"
                                    onChange={e => this.setState({combe: e.target.value})}
                                />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="6">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox1" name="hipertension" checked={this.state.hipertension} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox1">Hipertensión</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox2" name="tbcpulmonar" checked={this.state.tbcpulmonar} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox2">TBC Pulmonar</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox3" name="diabetes" checked={this.state.diabetes} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox3">Diabetes</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox4" name="pmamaria" checked={this.state.pmamaria} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox4">Patología mamaria</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox5" name="cardiopatias" checked={this.state.cardiopatias} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox5">CardioPatías</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox6" name="ets" checked={this.state.ets} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox6">E.T.S.</Label>
                            </FormGroup>                       
                        </Col>

                        <Col md="6">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox7" name="varices" checked={this.state.varices} onChange={this.cambiaEstadoCheckBox} />
                                <Label check className="form-check-label" htmlFor="checkbox7">Varices</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox8" name="erenal" checked={this.state.erenal} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox8">Enf. Renal</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox9" name="hepatopatias" checked={this.state.hepatopatias} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox9">Hepatopatías</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox10" name="tgenitales" checked={this.state.tgenitales} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox10">Tumores Genitales</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox11" name="egastrointestinal" checked={this.state.egastrointestinal} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox11">Enf. Gastrointestinal</Label>
                            </FormGroup>                       
                        </Col>
                        <Col md="1">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox12" name="otros"/>
                                <Label check className="form-check-label" htmlFor="checkbox12">Otros</Label>
                            </FormGroup>     
                        </Col>
                        <Col>
                            <FormGroup>
                                <Input 
                                    type="text"
                                    onChange={e => this.setState({otros: e.target.value})}
                                />
                            </FormGroup>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="1">
                            <Label>Antecedentes Quirúrgicos</Label>
                        </Col>
                        <Col md="8">
                            <Input
                                type="text"
                                placeholder="Antecedentes Quirúrgicos"
                                onChange={e => this.setState({aquirurgicos: e.target.value})}
                            />
                        </Col>
                        <Col md="1">
                            <Label>Año</Label>
                        </Col>
                        <Col md="2">
                            <Input 
                                type="number"
                                defaultValue={this.state.aquirurgicosano}
                                onChange={e => this.setState({aquirurgicosano: e.target.value})}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col md="1">
                            <Label>Alergias</Label>
                        </Col>
                        <Col md="11">
                            <Input
                                type="text"
                                placeholder="Alergias"
                                onChange={e => this.setState({alergias: e.target.value})}
                            />
                        </Col>
                    </FormGroup>

                    <Label>Transfusiones</Label>
                    <FormGroup row>
                        <Col md="1">
                            <Label>Año</Label>
                        </Col>
                        <Col md="2">
                            <Input 
                                type="number"
                                defaultValue={this.state.transfusionesano}
                                onChange={e => this.setState({transfusionesano: e.target.value})}
                            />
                        </Col>
                        <Col md="1">
                            <Label>Descripcion</Label>
                        </Col>
                        <Col md="8">
                            <Input
                                type="text"
                                placeholder="Alergias"
                                onChange={e => this.setState({transfusionesdes: e.target.value})}
                            />
                        </Col>
                    </FormGroup>

                    <h4>ANTECEDENTES FAMILIARES</h4>

                    <FormGroup row>
                        <Col md="6">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox21" name="hipertensionf" checked={this.state.hipertensionf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox21">Hipertensión</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox22" name="tbcpulmonarf" checked={this.state.tbcpulmonarf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox22">TBC Pulmonar</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox23" name="diabetesf" checked={this.state.diabetesf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox23">Diabetes</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox24" name="pmamariaf" checked={this.state.pmamariaf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox24">Patología mamaria</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox25" name="cardiopatiasf" checked={this.state.cardiopatiasf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox25">CardioPatías</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox26" name="etsf" checked={this.state.etsf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox26">E.T.S.</Label>
                            </FormGroup>                       
                        </Col>

                        <Col md="6">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox27" name="varicesf" checked={this.state.varicesf} onChange={this.cambiaEstadoCheckBox} />
                                <Label check className="form-check-label" htmlFor="checkbox27">Varices</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox28" name="erenalf" checked={this.state.erenalf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox28">Enf. Renal</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox29" name="hepatopatiasf" checked={this.state.hepatopatiasf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox29">Hepatopatías</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox210" name="tgenitalesf" checked={this.state.tgenitalesf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox210">Tumores Genitales</Label>
                            </FormGroup>                       
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox211" name="egastrointestinalf" checked={this.state.egastrointestinalf} onChange={this.cambiaEstadoCheckBox}/>
                                <Label check className="form-check-label" htmlFor="checkbox211">Enf. Gastrointestinal</Label>
                            </FormGroup>                       
                        </Col>
                        <Col md="1">
                            <FormGroup check className="checkbox">
                                <Input className="form-check-input" type="checkbox" id="checkbox212" name="otros"/>
                                <Label check className="form-check-label" htmlFor="checkbox212">Otros</Label>
                            </FormGroup>     
                        </Col>
                        <Col>
                            <FormGroup>
                                <Input 
                                    type="text"
                                    onChange={e => this.setState({otrosf: e.target.value})}
                                />
                            </FormGroup>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md="1">
                            <Label>Observaciones</Label>
                        </Col>
                        <Col md="11">
                            <Input
                                type="text"
                                placeholder="Obs."
                                onChange={e => this.setState({observacionesf: e.target.value})}
                            />
                        </Col>
                    </FormGroup>

                    <h4>ANTECEDENTES GINECO OBSTÉTRICOS</h4>
                    
                    <FormGroup row>
                        <Col md="3">
                            <FormGroup row>
                                <Col md="2">
                                    <Label>Gesta</Label>
                                </Col>
                                <Col md="8">
                                    <Input
                                        type="number"
                                        onChange={e => this.setState({gesta: e.target.value})}
                                    />
                                </Col>
                                <Col md="2">
                                    <Label>.</Label>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col md="2">
                                    <Label>Menarca</Label>
                                </Col>
                                <Col md="8">
                                    <Input
                                        type="number"
                                        onChange={e => this.setState({menarca: e.target.value})}
                                    />
                                </Col>
                                <Col md="2">
                                    <Label>años.</Label>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col md="3">
                            <FormGroup row>
                                <Col md="2">
                                    <Label>Para</Label>
                                </Col>
                                <Col md="10">
                                    <Input
                                        type="text"
                                        onChange={e => this.setState({para: e.target.value})}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col md="2">
                                    <Label>F.U.M.</Label>
                                </Col>
                                <Col md="10">
                                    <Input
                                        type="date"
                                        onChange={e => this.setState({fum: e.target.value})}
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col md="3">
                            <FormGroup row>
                                <Col md="3">
                                    <Label>Abortos</Label>
                                </Col>
                                <Col md="7">
                                    <Input
                                        type="number"
                                        onChange={e => this.setState({abortos: e.target.value})}
                                    />
                                </Col>
                                <Col md="1">
                                    <Label>.</Label>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col md="3">
                                    <Label>Catamenio</Label>
                                </Col>
                                <Col md="7">
                                    <Input
                                        type="number"
                                        onChange={e => this.setState({catamenio: e.target.value})}
                                    />
                                </Col>
                                <Col md="1">
                                    <Label>.</Label>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col md="3">
                            <FormGroup row>
                                <Col md="2">
                                    <Label>I.R.S</Label>
                                </Col>
                                <Col md="8">
                                    <Input
                                        type="number"
                                        onChange={e => this.setState({irs: e.target.value})}
                                    />
                                </Col>
                                <Col md="2">
                                    <Label>años.</Label>
                                </Col>
                            </FormGroup>
                        </Col>
                    </FormGroup>

                    </CardBody>
                    <CardFooter>
                        <Button onClick={this.enviarInformacionPromise} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                        <Button onClick={this.prueba1} type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
          </div>
        );
    }
}
export default HistoriaClinica;