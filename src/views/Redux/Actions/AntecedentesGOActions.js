export const UPDATE_ANTECEDENTESGO = 'antecedentesgos:updateAntecedentesGO';

export function updateAntecedentesGO(newAntecedentesGO){
    return{
        type: UPDATE_ANTECEDENTESGO,
        payload: {
            antecedentesgo: newAntecedentesGO
        }
    }
}