import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import renderHTML from 'react-render-html';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import { connect } from 'react-redux';
import Encabezado from '../Encabezado/Encabezado';

class HojaConsentimientoQ extends Component {
    constructor(props){
        super(props)

        var hoy = new Date(),
            f = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
        this.state = {
            fecha: f
        }
        this.handleChange = this.handleChange.bind(this);
    }

    

    handleChange(value){
        this.setState({text: value})
    }

    render() { 
        let nombre=this.props.paciente.primer_apellido+' '+this.props.paciente.segundo_apellido+' '+this.props.paciente.nombres;
        nombre = nombre.toLowerCase();
        return (
            <div className="animated fadeIn" class="historiaclinica" style={{textAlign: 'justify'}}>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardBody>
                            <Encabezado />
                            <div class="consentimiento">
                            <div class="thistoria"><h2><b>HOJA DE CONSENTIMIENTO INFORMADO</b></h2></div>
                                <p>
                                    Area de hospitalización: <b>SSU</b>. Especialidad: Cirugía
                                </p>
                                <p>
                                    Para satisfacción de los derechos del paciente como instrumento favorecedor del correcto y adecuado uso de los 
                                    procedimientos diagnóstico y terapéutico y en cumplimiento en normas y reglamento de la SEGURIDAD SOCIAL BOLIVIANA.
                                </p>
                                <p>
                                    Yo {this.props.paciente.id_sexo != 'F' ? <span>señor</span> : <span>señora</span>} <span style={{textTransform: 'capitalize'}}><b>{nombre}</b></span> como paciente con carnet de
                                    Identidad: <b>{this.props.paciente.dip}</b>, matrícula de asegurado: <b>{this.props.paciente.matricula}</b>.
                                </p>
                                <p>
                                    Yo señor [Sin asignar] familiar, tutor o representante
                                </p>
                                <p>
                                    En pleno uso de nuestras facultades libre y voluntariamente EXPONEMOS que fuimos debidamente INFORMADOS
                                    por el Dr. [Nombre Medico]. Especialidad: [Especialidad]
                                    que en entrevista personal realizada el día <b>{this.state.fecha}</b> nos informó que es necesario que me 
                                    <b> INTERNE</b> en la clínica del Seguro Social Universtario por cursar con: <b>{this.props.historialclinico.diagnostico_presuntivo} </b>
                                    y que he recibido explicaciones claras, sencillas y comprensibles sobre mi enfermedad y que he entendido y estoy satisfecho con ELLAS con las debidas aclaraciones y complicaciones con
                                    relación al procedimiento MEDICO y/o QUIRÚRGICO que se me efectuara y             <b>OTORGO MI CONSENTIMIENTO </b> 
                                    para que se me realice el procedimiento denominado: [PLAN QUIRURGICO]. Entendiendo y soy consciente de las complicaciones y/o efectos secundarios de dicho procedimiento y que este
                                    consentimiento puede ser <b>REVOCADO</b> por mí en cualquier momento antes de la realización.
                                </p>
                                <p>
                                    Y para que así conste firmaremos al pie del presente documento:
                                </p>
                            </div>
                            <div style={{height: '6cm'}}>
                                
                            </div>
                            {/* <ReactQuill 
                                modules={HojaConsentimientoQ.modules}
                                formats={HojaConsentimientoQ.formats}
                                value={this.state.text}
                                placeholder="Body"
                                onChange={this.handleChange}
                                size="lg"
                            />  */}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                          
            </div>
        );
    }
}

HojaConsentimientoQ.modules = {
    toolbar: [
        [{header: '1'}, {header: '2'}, {'font': []}],
        [{size: []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'color': []}, {'background': []}, {'align': []}],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        ['link', 'image'],
        ['clean'],
    ]
};

HojaConsentimientoQ.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'color', 'background', 'align',
    'list', 'bullet',
    'link', 'image', 'video', 'code-block'
]

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico
});

export default connect(mapStateToProps)(HojaConsentimientoQ);