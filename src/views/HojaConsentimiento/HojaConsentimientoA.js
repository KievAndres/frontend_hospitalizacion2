import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import renderHTML from 'react-render-html';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import { connect } from 'react-redux';
import Encabezado from '../Encabezado/Encabezado';

class HojaConsentimientoA extends Component {
    constructor(props){
        super(props)

        var hoy = new Date(),
            f = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
        this.state = {
            fecha: f
        }
        this.handleChange = this.handleChange.bind(this);
    }

    

    handleChange(value){
        this.setState({text: value})
    }

    render() { 
        let nombre=this.props.paciente.primer_apellido+' '+this.props.paciente.segundo_apellido+' '+this.props.paciente.nombres;
        nombre = nombre.toLowerCase();
        return (
            <div className="animated fadeIn" class="historiaclinica" style={{textAlign: 'justify'}}>
                <Row>
                    <Col md="12">
                        <Card>
                            <CardBody>
                            <Encabezado />
                            <div class="consentimiento">
                            <div class="thistoria"><h2><b>HOJA DE CONSENTIMIENTO INFORMADO ANESTESICO</b></h2></div>
                                <p>
                                    Para satisfacción de los derechos del paciente como instrumento favorecedor del correcto y adecuado uso de los 
                                    procedimientos diagnóstico y terapéutico y en cumplimiento en normas y reglamento de la SEGURIDAD SOCIAL BOLIVIANA.
                                </p>
                                <p>
                                    Yo {this.props.paciente.id_sexo != 'F' ? <span>señor</span> : <span>señora</span>} <span style={{textTransform: 'capitalize'}}><b>{nombre}</b></span> como paciente con carnet de
                                    Identidad: <b>{this.props.paciente.dip}</b>, matrícula de asegurado: <b>{this.props.paciente.matricula}</b>.
                                </p>
                                <p>
                                    Yo señor [Sin asignar] familiar, tutor o representante
                                </p>
                                <p>
                                    En pleno uso de nuestras facultades libre y voluntariamente EXPONEMOS que fuimos debidamente INFORMADOS
                                    por el Dr. [Nombre Medico]. Especialidad: [Especialidad]
                                    que en entrevista personal realizada el día <b>{this.state.fecha}</b> nos informó que es necesario que me 
                                    efectúe el procedimeinto anestésico denominado: [Procedimiento]
                                </p>
                                <p>
                                    Que he recibido explicaciones tanto verbal como escritas sobre la naturaleza y propósito del procedimiento
                                    terapéutico a seguir, beneficios, riesgos, alternativas y medios con las cuentas <b>EL SEGURO SOCIAL UNIVERSITARIO Y SU CLINICA </b>
                                    Para su realización habiendo tenido ocasión de aclarar las dudas e inquietudes que han surgido
                                </p>
                                <p>
                                    MANIFIESTO
                                </p>
                                <p>
                                    Que he entendido y estoy {this.props.paciente.id_sexo != 'F' ? <span>satisfecho</span> : <span>satisfecha</span>} de todas las aplicaciones y aclaraciones recibidas sobre el proceso
                                    médico quirúrgico citado y OTORGO MI CONSENTIMIENTO para que sea realizado el procedimiento indicado: [PROCEDIMIENTO]
                                    Entendiendo que este consentimiento puede ser <b>REVOCADO</b> por mí en cualquier momento antes de la realización del procedimiento quirúrgico referido.
                                </p>
                                <p>
                                    Y para que así conste firmaremos al pie del presente documento:
                                </p>
                            </div>
                            <div style={{height: '6cm'}}>
                                
                            </div>
                            {/* <ReactQuill 
                                modules={HojaConsentimientoQ.modules}
                                formats={HojaConsentimientoQ.formats}
                                value={this.state.text}
                                placeholder="Body"
                                onChange={this.handleChange}
                                size="lg"
                            />  */}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                          
            </div>
        );
    }
}

HojaConsentimientoA.modules = {
    toolbar: [
        [{header: '1'}, {header: '2'}, {'font': []}],
        [{size: []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'color': []}, {'background': []}, {'align': []}],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        ['link', 'image'],
        ['clean'],
    ]
};

HojaConsentimientoA.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'color', 'background', 'align',
    'list', 'bullet',
    'link', 'image', 'video', 'code-block'
]

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico
});

export default connect(mapStateToProps)(HojaConsentimientoA);